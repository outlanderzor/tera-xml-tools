﻿using DryIoc;
using TeraXmlTools.Common.Repositories;

namespace TeraXmlTools.InMemoryData.Prism.DryIoc.DependencyInjection;

internal sealed class UnitOfWorkFactory : IUnitOfWorkFactory
{
    private readonly IContainer container;

    public UnitOfWorkFactory(IContainer container)
    {
        this.container = container;
    }

    public IUnitOfWork CreateUnitOfWork()
    {
        return this.container.Resolve<IUnitOfWork>();
    }
}