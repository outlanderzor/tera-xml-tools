﻿using DryIoc;
using TeraXmlTools.Common.Repositories;
using TeraXmlTools.InMemoryData.Client.DataSource;
using TeraXmlTools.InMemoryData.Server.DataSource;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData.Prism.DryIoc.DependencyInjection;

public static class DependencyInjectionExtensions
{
    public static IRegistrator AddInMemoryData(this IRegistrator container)
    {
        container.Register<DataLoader>(Reuse.Singleton);
        container.Register<IUnitOfWorkFactory, UnitOfWorkFactory>();
        container.Register<IUnitOfWork, UnitOfWork>();
        container.Register<IFileTransaction, FileTransaction>();

        container.Register<ClientValueFactory>(Reuse.Singleton);
        container.Register<ServerValueFactory>(Reuse.Singleton);

        container.Register<ClientDataSource>(Reuse.Singleton);
        container.Register<ServerDataSource>(Reuse.Singleton);

        //// Server
        //container.Register<IServerQuestCompensationDataRepository, ServerQuestCompensationDataRepository>();

        //// Client
        //container.Register<IClientQuestRepository, ClientQuestRepository>();
        //container.Register<IClientStrSheetRepository, ClientStrSheetRepository>();

        return container;
    }
}