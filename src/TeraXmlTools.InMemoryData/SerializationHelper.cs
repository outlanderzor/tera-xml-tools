﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using TeraXmlTools.Domain;

namespace TeraXmlTools.InMemoryData;

public static class SerializationHelper
{
    public static IDictionary<string, string> GetNamespaces(Stream stream)
    {
        var xdocument = XDocument.Load(stream);
        var nav = xdocument.CreateNavigator();
        nav.MoveToFollowing(XPathNodeType.Element);
        var namespaces = nav.GetNamespacesInScope(XmlNamespaceScope.All);
        return namespaces ?? new Dictionary<string, string>();
    }

    public static T Deserialize<T>(FileStream fileStream)
    {
        var xmlSerializer = new XmlSerializer(typeof(T));
        var data = (T)xmlSerializer.Deserialize(fileStream);
        if (data is not RootModel rootModel) return data;
        fileStream.Seek(0, SeekOrigin.Begin);
        rootModel.FileName = fileStream.Name;
        return data;
    }

    public static void Serialize(Stream stream, Type dataType, object data)
    {
        const string clientNamespace = "TeraXmlTools.Domain.Client";
        var indentChars = dataType.FullName?.Contains(clientNamespace) ?? false
            ? new string(' ', 4)
            : new string(' ', 2);
        var xmlWritter = XmlWriter.Create(stream, new XmlWriterSettings
        {
            OmitXmlDeclaration = true,
            CloseOutput = false,
            Indent = true,
            IndentChars = indentChars
        });
        var xmlSerializer = new XmlSerializer(dataType);
        xmlSerializer.Serialize(xmlWritter, data);
    }
}