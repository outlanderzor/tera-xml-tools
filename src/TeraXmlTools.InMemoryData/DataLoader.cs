﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Mapster;

namespace TeraXmlTools.InMemoryData;

/// <summary>
/// This is the primary model loader for the in memory datasource.
/// It keeps track of the model objects for in memory updates when FileTransactions are commited.
/// </summary>
public sealed class DataLoader
{
    private readonly ConcurrentDictionary<string, WeakReference> loadedObjects = new();

    /// <summary>
    /// Update datasource objects loaded in memory.
    /// </summary>
    /// <param name="objects"></param>
    public void UpdateObjects(IDictionary<string, object> objects)
    {
        foreach (var kvp in objects)
            this.UpdateObject(kvp.Key, kvp.Value);
    }

    private void UpdateObject(string path, object updatedObject)
    {
        if (!this.loadedObjects.ContainsKey(path))
            return;
        var weakRef = this.loadedObjects[path];
        if (!weakRef.IsAlive)
        {
            this.loadedObjects.TryRemove(path, out _);
            return;
        }
        updatedObject.Adapt(weakRef.Target);
    }

    public async Task<IList<T>> DeserializeDirectory<T>(string directory, string? fileNameFilter)
        where T : class
    {
        var files = Directory.GetFiles(directory);
        if (fileNameFilter != null)
            files = files.Where(file => file.Contains(fileNameFilter)).ToArray();

        switch (files.Length)
        {
            case 0:
                return Array.Empty<T>();
            case 1:
            {
                var path = files[0];
                var data = SerializationHelper.Deserialize<T>(File.Open(files[0], FileMode.Open));
                if (!this.loadedObjects.ContainsKey(path))
                    this.loadedObjects.TryAdd(path, new WeakReference(data));
                return new List<T>(new[] { data });
            }
            default:
            {
                var tasks = Enumerable.Range(0, files.Length)
                    .Select(i => Task.Run(() =>
                    {
                        var path = files[i];
                        var data = SerializationHelper.Deserialize<T>(File.Open(path, FileMode.Open));
                        if (!this.loadedObjects.ContainsKey(path))
                            this.loadedObjects.TryAdd(path, new WeakReference(data));
                        return data;
                    }))
                    .ToArray();

                return await Task.WhenAll(tasks);
            }
        }
    }
}