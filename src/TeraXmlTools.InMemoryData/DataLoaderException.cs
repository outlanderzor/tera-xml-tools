﻿using System;

namespace TeraXmlTools.InMemoryData;

public sealed class DataLoaderException : Exception
{
    public DataLoaderException()
    {
    }

    public DataLoaderException(string message) : base(message)
    {
    }
}