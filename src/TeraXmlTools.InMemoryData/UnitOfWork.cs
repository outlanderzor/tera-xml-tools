﻿using System;
using System.Threading.Tasks;
using TeraXmlTools.Common.Repositories;
using TeraXmlTools.Common.Repositories.Client;
using TeraXmlTools.Common.Repositories.Server;
using TeraXmlTools.InMemoryData.Client.DataSource;
using TeraXmlTools.InMemoryData.Client.Repositories;
using TeraXmlTools.InMemoryData.Server.DataSource;
using TeraXmlTools.InMemoryData.Server.Repositories;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData;

public sealed class UnitOfWork : IUnitOfWork
{
    private readonly ClientDataSource clientDataSource;
    private readonly ServerDataSource serverDataSource;
    private readonly IFileTransaction fileTransaction;
    private readonly DataLoader dataLoader;

    private IClientQuestRepository? clientQuest;
    private IClientQuestCompensationDataRepository? clientQuestCompensationData;
    private IClientStrSheetRepository? clientStrSheet;
    private IServerQuestCompensationDataRepository? serverQuestCompensationData;

    public UnitOfWork(
        ClientDataSource clientDataSource, 
        ServerDataSource serverDataSource, 
        IFileTransaction fileTransaction,
        DataLoader dataLoader)
    {
        this.clientDataSource = clientDataSource;
        this.serverDataSource = serverDataSource;
        this.fileTransaction = fileTransaction;
        this.dataLoader = dataLoader;
    }

    public async Task SaveChangesAsync()
    {
        await this.fileTransaction.CommitAsync();
        this.dataLoader.UpdateObjects(this.fileTransaction.Log);
    }

    #region Client
    public IClientQuestCompensationDataRepository ClientQuestCompensationData
        => this.clientQuestCompensationData ??= new ClientQuestCompensationDataRepository(this.clientDataSource, this.fileTransaction);

    public IClientQuestRepository ClientQuest
        => this.clientQuest ??= new ClientQuestRepository(this.clientDataSource, this.fileTransaction);

    public IClientStrSheetRepository ClientStrSheet
        => this.clientStrSheet ??= new ClientStrSheetRepository(this.clientDataSource, this.fileTransaction);

    #endregion

    #region Server
    public IServerQuestCompensationDataRepository ServerQuestCompensationData
        => this.serverQuestCompensationData ??= new ServerQuestCompensationDataRepository(this.serverDataSource, this.fileTransaction);
    #endregion

    #region IDisposable
    ~UnitOfWork() { }

    public void Dispose(bool disposing)
    {
        if (disposing)
        {
            this.fileTransaction.Dispose();
        }
    }

    public void Dispose()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
    }
    #endregion

    #region IAsyncDisposable
    public ValueTask DisposeAsync()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
        return new ValueTask();
    }
    #endregion

}