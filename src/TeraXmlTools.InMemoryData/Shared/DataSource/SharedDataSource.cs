﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Domain.Server.QuestCompensationData;

namespace TeraXmlTools.InMemoryData.Shared.DataSource;

public interface ISharedValueFactory
{
    Task<IList<QuestCompensationData>> GetQuestCompensationData();
}

public abstract class SharedDataSource : ISharedDataSource
{
    private readonly ISharedValueFactory valueFactory;

    private Task<IList<QuestCompensationData>>? questCompensationData;

    protected SharedDataSource(ISharedValueFactory valueFactory)
    {
        this.valueFactory = valueFactory;
    }

    Task<IList<QuestCompensationData>> ISharedDataSource.QuestCompensationData => this.questCompensationData ??= this.valueFactory.GetQuestCompensationData();
}