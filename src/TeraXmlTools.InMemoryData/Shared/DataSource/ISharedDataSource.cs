﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Domain.Server.QuestCompensationData;

namespace TeraXmlTools.InMemoryData.Shared.DataSource;

public interface ISharedDataSource
{
    Task<IList<QuestCompensationData>> QuestCompensationData { get; }
}