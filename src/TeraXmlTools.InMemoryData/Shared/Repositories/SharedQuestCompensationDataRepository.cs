﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using TeraXmlTools.Common.Repositories;
using TeraXmlTools.Common.Repositories.Shared;
using TeraXmlTools.Domain.Server.QuestCompensationData;
using TeraXmlTools.InMemoryData.Shared.DataSource;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData.Shared.Repositories;

public abstract class SharedQuestCompensationDataRepository : ISharedQuestCompensationDataRepository
{
    private readonly ISharedDataSource dataSource;

    protected SharedQuestCompensationDataRepository(ISharedDataSource dataSource, IFileTransaction transaction)
    {
        this.Transaction = transaction;
        this.dataSource = dataSource;
    }

    public IFileTransaction Transaction { get; }

    public async Task<string> GetQuestCompensationDataFileName(int questId)
    {
        var data = await this.dataSource.QuestCompensationData;
        var quest = data.FirstOrDefault(cd => cd.Quests.Exists(q => q.QuestId == questId));
        if (quest == null)
            throw new RepositoryException($"Quest '{questId}' not found.");
        if (quest.FileName == null)
            throw new RepositoryException($"FileName is null.");
        return quest.FileName;
    }

    public async Task UpdateQuestCompensationData<T>(int questId, long? questExp, long? questGold, List<Item> items)
        where T : class, new()
    {
        var fileName = await this.GetQuestCompensationDataFileName(questId);

        // The transaction deserialize the file and keeps reference of the object being returned.
        var questCompensationData = await this.Transaction.OpenFileAsync<T>(fileName);
        
        // Copy the object using the server model as the destination type to enable the usage of the same method to update client and server data.
        // The server model is used since it's a complete version of the client model.
        // This is required because the client have less properties and diferent rules for serialization.
        var snapshot = questCompensationData.Adapt<QuestCompensationData>();

        var quest = snapshot.Quests.Single(q => q.QuestId == questId);
        var compensation = quest.Compensations?.FirstOrDefault();
        if (compensation == null)
        {
            compensation = new Compensation { CompensationId = 1 };
            quest.Compensations = new List<Compensation> { compensation };
        }
        var compensationType = compensation.CompensationTypes?.FirstOrDefault();
        if (compensationType == null)
        {
            compensationType = new CompensationType();
            compensation.CompensationTypes = new List<CompensationType> { compensationType };
        }
        compensationType.Exp = questExp.ToString();
        compensationType.Gold = questGold.ToString();
        compensationType.Items = items;

        // Update the original object that's being kept by the transaction.
        snapshot.Adapt(questCompensationData);
    }
}