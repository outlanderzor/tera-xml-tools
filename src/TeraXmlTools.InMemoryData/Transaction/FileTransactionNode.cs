﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace TeraXmlTools.InMemoryData.Transaction;

/// <summary>
/// Repesents a single file transaction in a linked transaction chain.
/// </summary>
internal sealed class FileTransactionNode : IDisposable
{
    private readonly FileStream sourceFileStream;
    private readonly FileStream snapshotFileStream;
    private readonly Type dataType;
    private bool disposed;
    private bool finalized;

    /// <summary>
    /// Created a new instance of FileTransactionNode.
    /// </summary>
    /// <param name="sourceFileStream">The original file stream.</param>
    /// <param name="snapshotFileStream">Backup stream in case of rollbacks.</param>
    /// <param name="dataType">The type of the root object from the xml file.</param>
    /// <param name="data">Reference of the deserialized object from the xml file.</param>
    /// <param name="next">The next node of the transaction chain.</param>
    private FileTransactionNode(FileStream sourceFileStream, FileStream snapshotFileStream, 
        Type dataType, object data, FileTransactionNode? next)
    {
        this.sourceFileStream = sourceFileStream;
        this.snapshotFileStream = snapshotFileStream;
        this.dataType = dataType;
        this.Data = data;
        this.Next = next;
        this.sourceFileStream.Lock(0, this.sourceFileStream.Length);
        this.snapshotFileStream.Lock(0, this.snapshotFileStream.Length);
    }

    public object Data { get; }

    public FileTransactionNode? Next { get; }

    public void Dispose()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
    }

    ~FileTransactionNode()
    {
    }

    private void Dispose(bool disposing)
    {
        if (disposing)
        {
            this.sourceFileStream.Dispose();
            this.snapshotFileStream.Dispose();
        }

        this.disposed = true;
    }

    private void ThrowIfDisposed()
    {
        if (this.disposed)
            throw new ObjectDisposedException(nameof(FileTransactionNode));
    }

    private void ThrowIfFinalized()
    {
        if (this.finalized)
            throw new InvalidOperationException("File transaction is finalized.");
    }

    public async Task RollbackAsync()
    {
        this.ThrowIfFinalized();
        this.sourceFileStream.SetLength(0);
        this.snapshotFileStream.Seek(0, SeekOrigin.Begin);
        await this.snapshotFileStream.CopyToAsync(this.sourceFileStream);
    }

    /// <summary>
    /// Start the transaction chain by serializing the current object into the sourceStream
    /// and then calling the CommitAsync of the next node if not null.
    /// </summary>
    /// <returns></returns>
    public async Task CommitAsync()
    {
        try
        {
            this.ThrowIfDisposed();
            this.ThrowIfFinalized();
            this.sourceFileStream.SetLength(0);
            SerializationHelper.Serialize(this.sourceFileStream, this.dataType, this.Data);
            if (this.Next != null)
                await this.Next.CommitAsync();
        }
        catch
        {
            await this.RollbackAsync();
            // ReThrow rollbacks all the commited transactions in the stack.
            throw;
        }
        finally
        {
            this.finalized = true;
            this.sourceFileStream.Close();
            this.snapshotFileStream.Close();
        }
    }

    public static async Task<FileTransactionNode> CreateAsync<T>(string path, FileTransactionNode? next)
        where T : class, new()
    {
        var tempFilePath = Path.GetTempFileName();
        var sourceFileStream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite, 1024);
        var snapshotFileStream = new FileStream(tempFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite, 1024);
        await sourceFileStream.CopyToAsync(snapshotFileStream);
        sourceFileStream.Seek(0, SeekOrigin.Begin);
        var data = SerializationHelper.Deserialize<T>(sourceFileStream);
        return new FileTransactionNode(sourceFileStream, snapshotFileStream, typeof(T), data, next);
    }
}