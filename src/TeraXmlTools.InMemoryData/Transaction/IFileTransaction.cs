﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TeraXmlTools.InMemoryData.Transaction;

/// <summary>
/// Enables atomic transactions with XML files by using the OpenFileAsync method.
/// 
/// </summary>
public interface IFileTransaction : IDisposable
{
    /// <summary>
    /// Lock and deserialize the file.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="path"></param>
    /// <returns></returns>
    Task<T> OpenFileAsync<T>(string path)
        where T : class, new();

    /// <summary>
    /// Files affected by the transaction.
    /// </summary>
    IDictionary<string, object> Log { get; }

    /// <summary>
    /// Commit changes to all files opened by this transaction.
    /// </summary>
    /// <returns></returns>
    Task CommitAsync();
}