﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TeraXmlTools.InMemoryData.Transaction;

public sealed class FileTransaction : IFileTransaction
{
    private readonly Dictionary<string, object> log = new();
    private FileTransactionNode? currentNode;
    private bool disposed;

    public async Task<T> OpenFileAsync<T>(string path)
        where T : class, new()
    {
        this.ThrowIfDisposed();
        var fileHandler = await FileTransactionNode.CreateAsync<T>(path, this.currentNode);
        this.currentNode = fileHandler;
        this.log.Add(path, fileHandler.Data);
        return (T)fileHandler.Data;
    }

    public IDictionary<string, object> Log => this.log;

    public async Task CommitAsync()
    {
        this.ThrowIfDisposed();
        if (this.currentNode != null)
            await this.currentNode.CommitAsync();
    }

    public void Dispose()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
    }

    ~FileTransaction()
    {
    }

    private void ThrowIfDisposed()
    {
        if (this.disposed)
            throw new ObjectDisposedException(nameof(FileTransaction));
    }

    private void Dispose(bool disposing)
    {
        try
        {
            if (!disposing) return;
            if (this.currentNode == null) return;
            var node = this.currentNode;
            do
            {
                node?.Dispose();
            } while ((node = node?.Next) != null);
        }
        finally
        {
            this.disposed = true;
        }
    }
}