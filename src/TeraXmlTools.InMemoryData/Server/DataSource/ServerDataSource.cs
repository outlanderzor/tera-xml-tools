﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Domain.Server.QuestCompensationData;
using TeraXmlTools.InMemoryData.Shared.DataSource;

namespace TeraXmlTools.InMemoryData.Server.DataSource;

public sealed class ServerDataSource : SharedDataSource
{
    private readonly ServerValueFactory valueFactory;

    private Task<IList<QuestCompensationData>>? questCompensationData;

    public ServerDataSource(ServerValueFactory valueFactory)
        : base(valueFactory)
    {
        this.valueFactory = valueFactory;
    }

    public Task<IList<QuestCompensationData>> QuestCompensationData => this.questCompensationData ??= this.valueFactory.GetQuestCompensationData();
}