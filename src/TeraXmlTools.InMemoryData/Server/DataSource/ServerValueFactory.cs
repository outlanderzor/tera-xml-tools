﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Common.Settings;
using TeraXmlTools.Domain.Server.QuestCompensationData;
using TeraXmlTools.InMemoryData.Shared.DataSource;

namespace TeraXmlTools.InMemoryData.Server.DataSource;

public sealed class ServerValueFactory : ISharedValueFactory
{
    private readonly IDataSettings settings;
    private readonly DataLoader dataLoader;

    public ServerValueFactory(IDataSettings settings, DataLoader dataLoader)
    {
        this.settings = settings;
        this.dataLoader = dataLoader;
    }

    async Task<IList<QuestCompensationData>> ISharedValueFactory.GetQuestCompensationData()
    {
        return await this.GetQuestCompensationData();
    }

    public Task<IList<QuestCompensationData>> GetQuestCompensationData()
    {
        var directory = $"{this.settings.DatasheetDirectory}\\CompensationData";
        return this.dataLoader.DeserializeDirectory<QuestCompensationData>(directory, "QuestCompensationData_");
    }
}