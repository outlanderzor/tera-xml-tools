﻿using TeraXmlTools.InMemoryData.Server.DataSource;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData.Server;

public abstract class ServerRepository
{
    protected ServerRepository(ServerDataSource dataSource, IFileTransaction transaction)
    {
        this.DataSource = dataSource;
        this.Transaction = transaction;
    }

    public ServerDataSource DataSource { get; }
    public IFileTransaction Transaction { get; }
}