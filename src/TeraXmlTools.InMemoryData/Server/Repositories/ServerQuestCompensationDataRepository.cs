﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Common.Repositories.Server;
using TeraXmlTools.Domain.Server.QuestCompensationData;
using TeraXmlTools.InMemoryData.Server.DataSource;
using TeraXmlTools.InMemoryData.Shared.Repositories;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData.Server.Repositories;

public sealed class ServerQuestCompensationDataRepository : SharedQuestCompensationDataRepository,
    IServerQuestCompensationDataRepository
{
    public ServerQuestCompensationDataRepository(ServerDataSource dataSource, IFileTransaction transaction)
        : base(dataSource, transaction)
    {
        this.DataSource = dataSource;
    }

    public ServerDataSource DataSource { get; }

    public Task<IList<QuestCompensationData>> GetQuestCompensationData()
    {
        return this.DataSource.QuestCompensationData;
    }
}