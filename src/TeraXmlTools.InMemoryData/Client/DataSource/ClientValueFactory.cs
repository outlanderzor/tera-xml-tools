﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Mapster;
using TeraXmlTools.Common.Settings;
using TeraXmlTools.Domain.Client.StrSheet;
using TeraXmlTools.Domain.Server.QuestCompensationData;
using TeraXmlTools.InMemoryData.Shared.DataSource;
using Quest = TeraXmlTools.Domain.Client.Quest.Quest;

namespace TeraXmlTools.InMemoryData.Client.DataSource;

public sealed class ClientValueFactory : ISharedValueFactory
{
    private readonly IDataSettings settings;
    private readonly DataLoader dataLoader;

    public ClientValueFactory(IDataSettings settings, DataLoader dataLoader)
    {
        this.settings = settings;
        this.dataLoader = dataLoader;
    }

    async Task<IList<QuestCompensationData>> ISharedValueFactory.GetQuestCompensationData()
    {
        var data = await this.GetQuestCompensationData();
        var adapted = data.Adapt<IList<QuestCompensationData>>();
        return adapted;
    }

    public Task<IList<Quest>> GetQuests()
    {
        var directory = $"{this.settings.DatacenterDirectory}\\Quest";
        return this.dataLoader.DeserializeDirectory<Quest>(directory, "Quest-");
    }

    public Task<IList<Domain.Client.QuestCompensationData.QuestCompensationData>> GetQuestCompensationData()
    {
        var directory = $"{this.settings.DatacenterDirectory}\\QuestCompensationData";
        return this.dataLoader.DeserializeDirectory<Domain.Client.QuestCompensationData.QuestCompensationData>(
            directory, "QuestCompensationData-");
    }

    public Task<IList<StrSheetItem>> GetStrSheetItems()
    {
        var directory = $"{this.settings.DatacenterDirectory}\\StrSheet_Item";
        return this.dataLoader.DeserializeDirectory<StrSheetItem>(directory, "StrSheet_Item-");
    }

    public Task<IList<StrSheetQuest>> GetStrSheetQuests()
    {
        var directory = $"{this.settings.DatacenterDirectory}\\StrSheet_Quest";
        return this.dataLoader.DeserializeDirectory<StrSheetQuest>(directory, "StrSheet_Quest-");
    }

    public Task<IList<StrSheetZoneName>> GetStrSheetZoneNames()
    {
        var directory = $"{this.settings.DatacenterDirectory}\\StrSheet_ZoneName";
        return this.dataLoader.DeserializeDirectory<StrSheetZoneName>(directory, "StrSheet_ZoneName-");
    }
}