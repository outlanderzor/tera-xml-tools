﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Domain.Client.QuestCompensationData;
using TeraXmlTools.Domain.Client.StrSheet;
using TeraXmlTools.InMemoryData.Shared.DataSource;
using Quest = TeraXmlTools.Domain.Client.Quest.Quest;

namespace TeraXmlTools.InMemoryData.Client.DataSource;

public sealed class ClientDataSource : SharedDataSource
{
    private readonly ClientValueFactory valueFactory;
    
    private Task<IList<Quest>>? quests;
    private Task<IList<QuestCompensationData>>? questCompensationDatas;
    private Task<IList<StrSheetItem>>? strSheetItems;
    private Task<IList<StrSheetQuest>>? strSheetQuest;
    private Task<IList<StrSheetZoneName>>? strSheetZoneName;

    public ClientDataSource(ClientValueFactory valueFactory)
        : base(valueFactory)
    {
        this.valueFactory = valueFactory;
    }

    public Task<IList<Quest>> Quests => this.quests ??= this.valueFactory.GetQuests();
    public Task<IList<QuestCompensationData>> QuestCompensationData => this.questCompensationDatas ??= this.valueFactory.GetQuestCompensationData();
    public Task<IList<StrSheetItem>> StrSheetItems => this.strSheetItems ??= this.valueFactory.GetStrSheetItems();
    public Task<IList<StrSheetQuest>> StrSheetQuests => this.strSheetQuest ??= this.valueFactory.GetStrSheetQuests();
    public Task<IList<StrSheetZoneName>> StrSheetZoneNames => this.strSheetZoneName ??= this.valueFactory.GetStrSheetZoneNames();
}