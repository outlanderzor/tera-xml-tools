﻿using TeraXmlTools.InMemoryData.Client.DataSource;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData.Client;

public abstract class ClientRepository
{
    protected ClientRepository(ClientDataSource dataSource, IFileTransaction transaction)
    {
        this.DataSource = dataSource;
        this.Transaction = transaction;
    }

    protected ClientDataSource DataSource { get; }
    protected IFileTransaction Transaction { get; }
}