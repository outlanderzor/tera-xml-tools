﻿using TeraXmlTools.Common.Repositories.Client;
using TeraXmlTools.InMemoryData.Client.DataSource;
using TeraXmlTools.InMemoryData.Shared.Repositories;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData.Client.Repositories;

public sealed class ClientQuestCompensationDataRepository : SharedQuestCompensationDataRepository,
    IClientQuestCompensationDataRepository
{
    public ClientQuestCompensationDataRepository(ClientDataSource dataSource, IFileTransaction transaction)
        : base(dataSource, transaction)
    {
        this.DataSource = dataSource;
    }

    public ClientDataSource DataSource { get; }
}