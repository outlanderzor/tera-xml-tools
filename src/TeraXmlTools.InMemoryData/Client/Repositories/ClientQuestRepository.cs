﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Common.Repositories.Client;
using TeraXmlTools.Domain.Client.Quest;
using TeraXmlTools.InMemoryData.Client.DataSource;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData.Client.Repositories;

public sealed class ClientQuestRepository : ClientRepository, IClientQuestRepository
{
    public ClientQuestRepository(ClientDataSource dataSource, IFileTransaction transaction) 
        : base(dataSource, transaction)
    {
    }

    public async Task<IList<Quest>> GetQuests()
    {
        var result = await this.DataSource.Quests;
        return result;
    }
}