﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeraXmlTools.Common.Repositories.Client;
using TeraXmlTools.Domain.Client.StrSheet;
using TeraXmlTools.InMemoryData.Client.DataSource;
using TeraXmlTools.InMemoryData.Transaction;

namespace TeraXmlTools.InMemoryData.Client.Repositories;

public sealed class ClientStrSheetRepository : ClientRepository, IClientStrSheetRepository
{
    public ClientStrSheetRepository(ClientDataSource dataSource, IFileTransaction transaction) 
        : base(dataSource, transaction)
    {
    }

    public Task<IList<StrSheetString>> GetItemStrings()
    {
        return Format(this.DataSource.StrSheetItems);
    }

    public Task<IList<StrSheetString>> GetQuestStrings()
    {
        return Format(this.DataSource.StrSheetQuests);
    }

    public Task<IList<StrSheetString>> GetZoneNameStrings()
    {
        var x = Format(this.DataSource.StrSheetZoneNames);
        return x;
    }

    private static async Task<IList<StrSheetString>> Format<T>(Task<IList<T>> strSheetCollection)
        where T : IStrSheet
    {
        var result = await strSheetCollection;
        if (result.Count == 0) return Array.Empty<StrSheetString>();
        return Enumerable.Range(0, result.Count)
            .SelectMany(i => result[i].String)
            .ToArray();
    }
}