﻿namespace TeraXmlTools.Common.Settings;

public interface IDataSettings
{
    string DatasheetDirectory { get; set; }
    string DatacenterDirectory { get; set; }
}