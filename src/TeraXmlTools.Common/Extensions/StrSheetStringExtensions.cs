﻿using System.Collections.Generic;
using System.Linq;
using TeraXmlTools.Domain.Client.StrSheet;

namespace TeraXmlTools.Common.Extensions;

public static class StrSheetStringExtensions
{
    public static IDictionary<int, string?> ToDictionary(this IEnumerable<StrSheetString> strings)
    {
        return strings.ToDictionary(s => s.Id, s => s.String);
    }
}