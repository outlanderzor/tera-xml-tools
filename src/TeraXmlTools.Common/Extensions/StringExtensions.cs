﻿using System.Linq;

namespace TeraXmlTools.Common.Extensions;

public static class StringExtensions
{
    public static string GetNumbers(this string input)
    {
        return new string(input.Where(char.IsDigit).ToArray());
    }
}