﻿namespace TeraXmlTools.Common.Repositories;

public interface IUnitOfWorkFactory
{
    public IUnitOfWork CreateUnitOfWork();
}