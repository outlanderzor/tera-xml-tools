﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Common.Repositories.Shared;
using TeraXmlTools.Domain.Server.QuestCompensationData;

namespace TeraXmlTools.Common.Repositories.Server;

public interface IServerQuestCompensationDataRepository : ISharedQuestCompensationDataRepository
{
    Task<IList<QuestCompensationData>> GetQuestCompensationData();
}