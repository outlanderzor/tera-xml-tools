﻿using System;
using System.Threading.Tasks;
using TeraXmlTools.Common.Repositories.Client;
using TeraXmlTools.Common.Repositories.Server;

namespace TeraXmlTools.Common.Repositories;

public interface IUnitOfWork : IDisposable, IAsyncDisposable
{
    #region Client Repositories
    IClientQuestCompensationDataRepository ClientQuestCompensationData { get; }
    IClientQuestRepository ClientQuest { get; }
    IClientStrSheetRepository ClientStrSheet { get; }
    #endregion

    #region Server Repositories
    IServerQuestCompensationDataRepository ServerQuestCompensationData { get; }
    #endregion

    Task SaveChangesAsync();
}