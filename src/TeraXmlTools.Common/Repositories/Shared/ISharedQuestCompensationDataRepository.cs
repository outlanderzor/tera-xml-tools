﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Domain.Server.QuestCompensationData;

namespace TeraXmlTools.Common.Repositories.Shared;

public interface ISharedQuestCompensationDataRepository
{
    Task<string> GetQuestCompensationDataFileName(int questId);
    Task UpdateQuestCompensationData<T>(int questId, long? questExp, long? questGold, List<Item> items)
        where T : class, new();
}