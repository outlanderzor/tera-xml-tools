﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Domain.Client.StrSheet;

namespace TeraXmlTools.Common.Repositories.Client;

public interface IClientStrSheetRepository
{
    Task<IList<StrSheetString>> GetItemStrings();
    Task<IList<StrSheetString>> GetQuestStrings();
    Task<IList<StrSheetString>> GetZoneNameStrings();
}