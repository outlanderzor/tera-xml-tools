﻿using TeraXmlTools.Common.Repositories.Shared;

namespace TeraXmlTools.Common.Repositories.Client;

public interface IClientQuestCompensationDataRepository : ISharedQuestCompensationDataRepository
{
}