﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TeraXmlTools.Domain.Client.Quest;

namespace TeraXmlTools.Common.Repositories.Client;

public interface IClientQuestRepository
{
    Task<IList<Quest>> GetQuests();
}