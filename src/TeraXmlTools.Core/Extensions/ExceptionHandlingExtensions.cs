﻿using System;
using Prism.Services.Dialogs;

namespace TeraXmlTools.Core.Extensions;

public static class ExceptionHandlingExtensions
{
    public static void HandleError(this IDialogService dialogService, Exception exception)
    {
        dialogService.ShowDialog(DialogNames.MessageBox.Name, new DialogParameters
        {
            {DialogNames.MessageBox.MessageParameter, exception.Message},
            {DialogNames.MessageBox.TitleParameter, "Error"}
        }, _ => {});
    }
}