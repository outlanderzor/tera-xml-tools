﻿namespace TeraXmlTools.Core;

public static class DialogNames
{
    public static class MessageBox
    {
        public const string Name = nameof(MessageBox);

        public const string MessageParameter = "message";
        public const string TitleParameter = "title";
    }
}