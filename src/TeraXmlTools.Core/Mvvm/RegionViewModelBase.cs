﻿using System;
using Prism.Regions;

namespace TeraXmlTools.Core.Mvvm;

public abstract class RegionViewModelBase : ViewModelBase, IConfirmNavigationRequest
{
    protected RegionViewModelBase(IRegionManager regionManager)
    {
        this.RegionManager = regionManager;
    }

    protected IRegionManager RegionManager { get; }

    public virtual void OnNavigatedTo(NavigationContext navigationContext)
    {
    }

    public virtual bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public virtual void OnNavigatedFrom(NavigationContext navigationContext)
    {
    }

    public virtual void ConfirmNavigationRequest(NavigationContext navigationContext, Action<bool> continuationCallback)
    {
        continuationCallback(true);
    }
}