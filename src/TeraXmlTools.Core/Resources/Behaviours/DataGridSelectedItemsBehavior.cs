﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Xaml.Behaviors;

namespace TeraXmlTools.Core.Resources.Behaviours;

public class DataGridSelectedItemsBehavior : Behavior<DataGrid>
{
    public static readonly DependencyProperty SelectedItemsOverrideProperty =
        DependencyProperty.Register(nameof(SelectedItemsOverride), typeof(IList),
            typeof(DataGridSelectedItemsBehavior), new FrameworkPropertyMetadata(null) { BindsTwoWayByDefault = false });

    public IList SelectedItemsOverride
    {
        get => (IList)this.GetValue(SelectedItemsOverrideProperty);
        set => this.SetValue(SelectedItemsOverrideProperty, value);
    }

    private void AssociatedObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (this.SelectedItemsOverride == null) return;
        this.SelectedItemsOverride.Clear();
        foreach (var item in this.AssociatedObject.SelectedItems) this.SelectedItemsOverride.Add(item);
    }


    protected override void OnAttached()
    {
        base.OnAttached();
        this.AssociatedObject.SelectionChanged += this.AssociatedObject_SelectionChanged;
    }

    protected override void OnDetaching()
    {
        base.OnDetaching();
        if (this.AssociatedObject == null) return;
        this.AssociatedObject.SelectionChanged -= this.AssociatedObject_SelectionChanged;
    }
}