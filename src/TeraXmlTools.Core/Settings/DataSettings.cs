﻿using TeraXmlTools.Common.Settings;

namespace TeraXmlTools.Core.Settings;

public sealed record DataSettings : IDataSettings
{
    public string DatasheetDirectory { get; set; }
    public string DatacenterDirectory { get; set; }
}