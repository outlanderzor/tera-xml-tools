﻿namespace TeraXmlTools.Core;

public static class RegionNames
{
    public const string MainContentRegion = nameof(MainContentRegion);
    public const string QuestEditorRegion = nameof(QuestEditorRegion);
}