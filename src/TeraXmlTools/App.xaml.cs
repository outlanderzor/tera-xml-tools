﻿using System.Windows;
using Prism.DryIoc;
using Prism.Ioc;
using Prism.Modularity;
using TeraXmlTools.Core;
using TeraXmlTools.InMemoryData.Prism.DryIoc.DependencyInjection;
using TeraXmlTools.Modules.Application;
using TeraXmlTools.ViewModels;
using TeraXmlTools.Views;

namespace TeraXmlTools;

/// <summary>
///     Interaction logic for App.xaml
/// </summary>
public partial class App
{
    protected override Window CreateShell()
    {
        return this.Container.Resolve<MainWindow>();
    }

    protected override void RegisterTypes(IContainerRegistry containerRegistry)
    {
        containerRegistry.GetContainer()
            .AddConfiguration()
            .AddInMemoryData()
            .AddMediatR();

        containerRegistry.RegisterDialog<MessageBoxView, MessageBoxViewModel>(DialogNames.MessageBox.Name);
    }

    protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
    {
        moduleCatalog.AddModule<ApplicationModule>();
    }
}