﻿using System.IO;
using System.Linq;
using DryIoc;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.Configuration;
using TeraXmlTools.Common.Settings;
using TeraXmlTools.Core.Settings;
using TeraXmlTools.Services;

namespace TeraXmlTools;

public static class DependencyInjectionExtensions
{
    public static IRegistrator AddMediatR(this IRegistrator container)
    {
        container.RegisterDelegate<ServiceFactory>(r => r.Resolve);
        container.RegisterMany(new[] { typeof(IMediator).GetAssembly() }, Registrator.Interfaces);
        var requestHandlerTypes = typeof(ServicesAssembly).GetAssembly().GetTypes()
            .Where(t => !t.IsAbstract &&
                        t.GetInterfaces()
                            .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IRequestHandler<,>)));
        container.RegisterMany(requestHandlerTypes);
        container.Register(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>),
            ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
        container.Register(typeof(IPipelineBehavior<,>), typeof(RequestPostProcessorBehavior<,>),
            ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
        return container;
    }

    public static IRegistrator AddConfiguration(this IRegistrator container)
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
        var configuration = builder.Build();

        var dataSettings = new DataSettings();
        configuration.GetSection(nameof(DataSettings)).Bind(dataSettings);

        container.RegisterInstance<IDataSettings>(dataSettings);
        return container;
    }
}