﻿using MediatR;
using Prism.Commands;
using Prism.Regions;
using TeraXmlTools.Core;
using TeraXmlTools.Core.Mvvm;
using TeraXmlTools.Modules.Application.Views;

namespace TeraXmlTools.ViewModels;

public class MainWindowViewModel : RegionViewModelBase
{
    private readonly ISender mediator;
    private string title = "Tera XML Tools";

    public MainWindowViewModel(ISender mediator, IRegionManager regionManager)
        : base(regionManager)
    {
        this.mediator = mediator;

        this.NavigateToQuestSelecionCommand =
            new DelegateCommand(() => this.RegionManager.RequestNavigate(RegionNames.MainContentRegion, nameof(QuestSelectionView)));
    }

    public string Title
    {
        get => this.title;
        set => this.SetProperty(ref this.title, value);
    }

    public DelegateCommand NavigateToQuestSelecionCommand { get; }

    public void Loaded()
    {
        //var response = await this.mediator.Send(new GetAllQuestsRequest());
        //foreach (var quest in response.SourceQuests)
        //{

        //    if (quest.QuestId == 1037)
        //        Console.WriteLine(quest);
        //}
        //await File.WriteAllLinesAsync("d:\\quests.csv", response.SourceQuests.Select(q => $"\"{q.QuestId}\",\"{q.QuestName}\",{q.MinimumLevel},{q.RecommendedLevel},\"{q.ZoneId}\",\"{q.ZoneName}\",\"{q.QuestExp}\",\"{q.QuestGold}\",\"{q.FileName}\""));
        //await Task.Delay(100);
    }
}