﻿using System;
using Prism.Commands;
using Prism.Services.Dialogs;
using TeraXmlTools.Core;
using TeraXmlTools.Core.Mvvm;

namespace TeraXmlTools.ViewModels;

public class MessageBoxViewModel : ViewModelBase, IDialogAware
{
    private DelegateCommand<string> closeDialogCommand;

    private string message;

    private string title = "Notification";

    public DelegateCommand<string> CloseDialogCommand => this.closeDialogCommand ??= new DelegateCommand<string>(this.CloseDialog);

    public string Message
    {
        get => this.message;
        set => this.SetProperty(ref this.message, value);
    }

    public string Title
    {
        get => this.title;
        set => this.SetProperty(ref this.title, value);
    }

    public event Action<IDialogResult> RequestClose;

    public bool CanCloseDialog()
    {
        return true;
    }

    public void OnDialogClosed()
    {
    }

    public void OnDialogOpened(IDialogParameters parameters)
    {
        this.Message = parameters.GetValue<string>(DialogNames.MessageBox.MessageParameter);
        this.Title = parameters.GetValue<string>(DialogNames.MessageBox.TitleParameter);
    }

    private void CloseDialog(string parameter)
    {
        var result = parameter?.ToLower() switch
        {
            "true" => ButtonResult.OK,
            "false" => ButtonResult.Cancel,
            _ => ButtonResult.None
        };

        this.RaiseRequestClose(new DialogResult(result));
    }

    public void RaiseRequestClose(IDialogResult dialogResult)
    {
        this.RequestClose?.Invoke(dialogResult);
    }
}