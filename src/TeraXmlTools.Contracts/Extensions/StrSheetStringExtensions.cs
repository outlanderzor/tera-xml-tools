﻿using System.Collections.Generic;
using System.Linq;
using TeraXmlTools.Contracts.Client.StrSheet;

namespace TeraXmlTools.Contracts.Extensions;

public static class StrSheetStringExtensions
{
    public static IDictionary<int, string?> ToDictionary(this IEnumerable<StrSheetString> strings)
    {
        return strings.ToDictionary(s => s.Id, s => s.String);
    }
}