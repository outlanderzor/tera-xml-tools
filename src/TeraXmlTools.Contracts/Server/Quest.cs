﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Server;

[XmlRoot(ElementName = "Quest")]
public sealed record Quest
{

    [XmlAttribute(AttributeName = "questId")]
    public int QuestId { get; set; }

    [XmlElement(ElementName = "Compensation")]
    public Compensation Compensation { get; set; }
}