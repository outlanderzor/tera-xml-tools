﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Server;

[XmlRoot(ElementName = "Compensation")]
public sealed record Compensation
{

    [XmlElement(ElementName = "CompensationType")]
    public CompensationType CompensationType { get; set; }

    [XmlAttribute(AttributeName = "compensationId")]
    public int CompensationId { get; set; }
}