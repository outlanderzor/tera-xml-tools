﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Server;

[XmlRoot(ElementName = "CompensationType")]
public sealed record CompensationType
{
    [XmlElement(ElementName = "Item")]
    public List<Item> Item { get; set; }

    [XmlAttribute(AttributeName = "type")]
    public string Type { get; set; }

    [XmlAttribute(AttributeName = "exp")]
    public string Exp { get; set; }

    [XmlAttribute(AttributeName = "gold")]
    public string Gold { get; set; }

    [XmlAttribute(AttributeName = "itemBag")]
    public string ItemBag { get; set; }

    [XmlAttribute(AttributeName = "memo")]
    public string Memo { get; set; }

    [XmlAttribute(AttributeName = "policyPoint")]
    public string PolicyPoint { get; set; }

    [XmlAttribute(AttributeName = "npcGuildId")]
    public string NpcGuildId { get; set; }

    [XmlAttribute(AttributeName = "reputationExp")]
    public string ReputationExp { get; set; }

    [XmlAttribute(AttributeName = "reputationPoint")]
    public string ReputationPoint { get; set; }
}