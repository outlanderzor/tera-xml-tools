﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Server;

[XmlRoot(ElementName = "Item")]
public sealed record Item
{

    [XmlAttribute(AttributeName = "templateId")]
    public int TemplateId { get; set; }

    [XmlAttribute(AttributeName = "quantity")]
    public int Quantity { get; set; }

    [XmlAttribute(AttributeName = "class")]
    public string Class { get; set; }
}