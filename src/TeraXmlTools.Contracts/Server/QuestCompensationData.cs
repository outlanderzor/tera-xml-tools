﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Server;

[XmlRoot(ElementName = "QuestCompensationData")]
public sealed record QuestCompensationData : FileSourceContract
{

    [XmlElement(ElementName = "Quest")]
    public List<Quest> Quest { get; set; }

    [XmlAttribute(AttributeName = "huntingZoneId")]
    public int HuntingZoneId { get; set; }
}

