﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts;

public abstract record FileSourceContract
{
    [XmlIgnore]
    public string FileSource { get; set; }
}