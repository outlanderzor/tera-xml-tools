﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "수행조건")]
public sealed record PerformanceCondition
{
    [XmlElement(ElementName = "길드레벨")]
    public string GuildLevel { get; set; }

    [XmlElement(ElementName = "선행퀘스트")]
    public PriorQuest PriorQuest { get; set; }

    [XmlElement(ElementName = "영주길드")]
    public string LordGuild { get; set; }

    [XmlElement(ElementName = "최소레벨")]
    public int MinimumLevel { get; set; }
}