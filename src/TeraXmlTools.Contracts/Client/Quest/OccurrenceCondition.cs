﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "발생조건")]
public sealed record OccurrenceCondition
{
    [XmlElement(ElementName = "즉시수주")]
    public int 즉시수주 { get; set; }
}