﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

// using System.Xml.Serialization;
// XmlSerializer serializer = new XmlSerializer(typeof(Quest));
// using (StringReader reader = new StringReader(xml))
// {
//    var test = (Quest)serializer.Deserialize(reader);
// }

[XmlRoot(ElementName = "Quest")]
public sealed record Quest
{
    [XmlElement(ElementName = "Header")]
    public Header Header { get; set; }

    [XmlElement(ElementName = "Tasks")]
    public QuestTasks QuestTasks { get; set; }

    //[XmlAttribute(AttributeName = "xsi")]
    //public string Xsi { get; set; }

    //[XmlAttribute(AttributeName = "schemaLocation")]
    //public string SchemaLocation { get; set; }

    [XmlAttribute(AttributeName = "id")]
    public int Id { get; set; }

    //[XmlAttribute(AttributeName = "xmlns")]
    //public string Xmlns { get; set; }

    [XmlText]
    public string Text { get; set; }
}