﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "오브젝트지정")]
public sealed record ObjectDesignationObjectId
{
    [XmlElement(ElementName = "동작횟수")]
    public int NumberOfOperations { get; set; }

    [XmlElement(ElementName = "오브젝트Id")]
    public int ObjectId { get; set; }
}