﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "Body")]
public sealed record Body
{
    [XmlElement(ElementName = "방문그룹")]
    public VisitingGroup VisitingGroup { get; set; }

    [XmlElement(ElementName = "보상")]
    public string Compensation { get; set; }

    [XmlElement(ElementName = "저널Text")]
    public string JournalText { get; set; }

    [XmlElement(ElementName = "몬스터지정")]
    public MonsterDesignation MonsterDesignation { get; set; }

    [XmlElement(ElementName = "진행조건")]
    public ProgressCondition ProgressCondition { get; set; }

    [XmlElement(ElementName = "목표지역")]
    public string TargetArea { get; set; }

    [XmlElement(ElementName = "제한시간")]
    public string TimeLimit { get; set; }

    [XmlElement(ElementName = "지역명")]
    public string LocalPeople { get; set; }

    [XmlElement(ElementName = "오브젝트지정")]
    public ObjectDesignation ObjectDesignation { get; set; }

    [XmlElement(ElementName = "대상NPC지정")]
    public string TargetNpcDesignation { get; set; }

    [XmlElement(ElementName = "호위횟수")]
    public int NumberOfEscorts { get; set; }
}