﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "진행조건")]
public sealed record ProgressCondition
{
    [XmlElement(ElementName = "사냥인원")]
    public string NumberOfHunters { get; set; }

    [XmlElement(ElementName = "제한시간")]
    public string TimeLimit { get; set; }
}