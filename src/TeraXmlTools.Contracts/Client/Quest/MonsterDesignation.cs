﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "몬스터지정")]
public sealed record MonsterDesignation
{
    public MonsterDesignationMonsterId MonsterDesignationMonsterId { get; set; }
}