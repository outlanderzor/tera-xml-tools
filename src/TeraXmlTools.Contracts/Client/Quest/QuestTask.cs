﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "Task")]
public sealed record QuestTask
{
    [XmlElement(ElementName = "Body")]
    public Body Body { get; set; }

    [XmlElement(ElementName = "Header")]
    public Header Header { get; set; }

    [XmlAttribute(AttributeName = "id")]
    public int Id { get; set; }

    [XmlText]
    public string Text { get; set; }
}