﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "Header")]
public sealed record Header
{
    [XmlElement(ElementName = "Quest번호")]
    public string QuestNumber { get; set; }

    [XmlElement(ElementName = "Quest제목")]
    public string QuestTitle { get; set; }

    [XmlElement(ElementName = "반복퀘스트")]
    public string RepeatQuest { get; set; }

    [XmlElement(ElementName = "발생조건")]
    public OccurrenceCondition OccurrenceCondition { get; set; }

    [XmlElement(ElementName = "수행조건")]
    public PerformanceCondition PerformanceCondition { get; set; }

    [XmlElement(ElementName = "수행지역")]
    public string PracticeArea { get; set; }

    [XmlElement(ElementName = "스토리그룹Id")]
    public string StoryGroupId { get; set; }

    [XmlElement(ElementName = "요약정보")]
    public int SummaryInformation { get; set; }

    [XmlElement(ElementName = "적정수행레벨")]
    public string AppropriatePerformanceLevel { get; set; }

    [XmlElement(ElementName = "적정수행인원")]
    public string AppropriateNumberOfParticipants { get; set; }

    [XmlElement(ElementName = "집회소분류")]
    public string MeetinghouseClassification { get; set; }

    [XmlElement(ElementName = "추가보상")]
    public string AdditionalCompensation { get; set; }

    [XmlElement(ElementName = "퀘스트종류")]
    public string QuestType { get; set; }

    [XmlElement(ElementName = "특수가이드")]
    public string SpecialGuide { get; set; }

    [XmlElement(ElementName = "ContinentId")]
    public string ContinentId { get; set; }

    [XmlElement(ElementName = "Id")]
    public int Id { get; set; }

    [XmlElement(ElementName = "JournalScript")]
    public int JournalScript { get; set; }

    [XmlElement(ElementName = "Pos")]
    public string Pos { get; set; }

    [XmlElement(ElementName = "다음Task")]
    public string NextTask { get; set; }

    [XmlElement(ElementName = "이름")]
    public string Name { get; set; }
}