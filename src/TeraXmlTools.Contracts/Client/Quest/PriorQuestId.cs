﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "선행퀘스트")]
public sealed record PriorQuestId
{
    [XmlElement(ElementName = "퀘스트Id")]
    public string QuestId { get; set; }
}