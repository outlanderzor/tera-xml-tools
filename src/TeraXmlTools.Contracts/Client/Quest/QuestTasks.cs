﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "Tasks")]
public sealed record QuestTasks
{
    [XmlElement(ElementName = "Task")]
    public List<QuestTask> Task { get; set; }
}