﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "몬스터지정")]
public sealed record MonsterDesignationMonsterId
{
    [XmlElement(ElementName = "몬스터Id")]
    public string? MonsterId { get; set; }

    [XmlElement(ElementName = "사냥마리수")]
    public int NumberOfHunts { get; set; }
}