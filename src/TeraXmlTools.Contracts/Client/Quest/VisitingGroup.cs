﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "방문그룹")]
public sealed record VisitingGroup
{
    [XmlElement(ElementName = "방문그룹")]
    public List<VisitingGroup> Groups { get; set; }

    [XmlElement(ElementName = "NPCId")]
    public string NpcId { get; set; }
}