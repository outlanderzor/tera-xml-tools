﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.Quest;

[XmlRoot(ElementName = "선행퀘스트")]
public sealed record PriorQuest
{
    [XmlElement(ElementName = "선행퀘스트")]
    public PriorQuestId PriorQuestId { get; set; }
}