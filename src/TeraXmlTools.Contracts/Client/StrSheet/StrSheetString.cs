﻿using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.StrSheet;

[XmlRoot(ElementName = "String")]
public sealed record StrSheetString
{
    [XmlAttribute(AttributeName = "id")]
    public int Id { get; set; }

    [XmlAttribute(AttributeName = "string")]
    public string? String { get; set; }

    [XmlAttribute(AttributeName = "readableId")]
    public string ReadableId { get; set; }

    [XmlText]
    public string Text { get; set; }
}