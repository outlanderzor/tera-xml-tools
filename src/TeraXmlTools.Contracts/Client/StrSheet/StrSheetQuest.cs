﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TeraXmlTools.Contracts.Client.StrSheet;

[XmlRoot(ElementName = "StrSheet_Quest")]
public sealed record StrSheetQuest : FileSourceContract, IStrSheet
{
    [XmlElement(ElementName = "String")]
    public List<StrSheetString> String { get; set; }

    //[XmlAttribute(AttributeName = "xsi")]
    //public string Xsi { get; set; }

    //[XmlAttribute(AttributeName = "schemaLocation")]
    //public string SchemaLocation { get; set; }

    //[XmlAttribute(AttributeName = "xmlns")]
    //public string Xmlns { get; set; }
}