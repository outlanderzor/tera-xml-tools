﻿using System.Collections.Generic;

namespace TeraXmlTools.Contracts.Client.StrSheet;

public interface IStrSheet
{
    List<StrSheetString> String { get; set; }
}