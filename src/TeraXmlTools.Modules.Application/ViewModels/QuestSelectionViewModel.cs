﻿using System;
using System.Linq;
using MediatR;
using Prism.Commands;
using Prism.Regions;
using TeraXmlTools.Common.Collections;
using TeraXmlTools.Core;
using TeraXmlTools.Core.Mvvm;
using TeraXmlTools.Modules.Application.Views;
using TeraXmlTools.Services.Quests.GetAllQuests;

namespace TeraXmlTools.Modules.Application.ViewModels;

public class QuestSelectionViewModel : RegionViewModelBase
{
    private readonly ISender mediator;
    private readonly IRegionManager regionManager;
    private string searchString;

    public QuestSelectionViewModel(ISender mediator, IRegionManager regionManager)
        : base(regionManager)
    {
        this.mediator = mediator;
        this.regionManager = regionManager;
        this.OpenSelectedQuestCommand = new DelegateCommand<QuestModel>(this.OpenSelectedQuest);
    }

    public string SearchString
    {
        get => this.searchString;
        set
        {
            this.SetProperty(ref this.searchString, value);
            this.ApplySearchFilter();
        }
    }

    public DelegateCommand<QuestModel> OpenSelectedQuestCommand { get; }
    public SmartCollection<QuestModel> SourceQuests { get; } = new();
    public SmartCollection<QuestModel> FilteredQuests { get; } = new();


    private void ApplySearchFilter()
    {
        var filteredItems = this.SourceQuests.Where(quest => string.IsNullOrWhiteSpace(this.SearchString)
                                                             || quest.QuestId.ToString().Contains(this.SearchString)
                                                             || quest.MinimumLevel.ToString()
                                                                 .Contains(this.SearchString)
                                                             || quest.RecommendedLevel.ToString()
                                                                 .Contains(this.SearchString)
                                                             || (quest.QuestName?.Contains(this.SearchString,
                                                                 StringComparison.InvariantCultureIgnoreCase) ?? false)
                                                             || (quest.ZoneName?.Contains(this.SearchString,
                                                                     StringComparison.InvariantCultureIgnoreCase) ??
                                                                 false));
        this.FilteredQuests.Reset(filteredItems);
    }

    public void OpenSelectedQuest(QuestModel questModel)
    {
        if (questModel == null) return;
        var navigationParams = new NavigationParameters
        {
            { QuestEditorViewModel.QuestModelParameter, questModel }
        };
        this.regionManager.RequestNavigate(RegionNames.QuestEditorRegion,
            new Uri(nameof(QuestEditorView), UriKind.Relative), navigationParams);
    }

    public async void Loaded()
    {
        var response = await this.mediator.Send(new GetAllQuestsRequest());
        this.SourceQuests.Reset(response.Quests
            .Where(q => q.QuestGold.HasValue || q.QuestExp.HasValue || !string.IsNullOrEmpty(q.QuestName))
            .OrderBy(q => q.QuestId));
        this.FilteredQuests.Reset(this.SourceQuests);
    }
}