﻿using TeraXmlTools.Core.Mvvm;

namespace TeraXmlTools.Modules.Application.ViewModels;

public class ViewAViewModel : ViewModelBase
{
    private string message;

    public ViewAViewModel()
    {
        this.Message = "View A from your Prism Module";
    }

    public string Message
    {
        get => this.message;
        set => this.SetProperty(ref this.message, value);
    }
}