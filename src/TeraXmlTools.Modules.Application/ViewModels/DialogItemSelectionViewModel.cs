﻿using System;
using System.Collections.Generic;
using System.Linq;
using MediatR;
using Prism.Commands;
using Prism.Services.Dialogs;
using TeraXmlTools.Common.Collections;
using TeraXmlTools.Core.Mvvm;
using TeraXmlTools.Services.Items.GetAllItems;

namespace TeraXmlTools.Modules.Application.ViewModels;

public class DialogItemSelectionViewModel : ViewModelBase, IDialogAware
{
    public const string SelectedItemsParameter = nameof(SelectedItems);
    private readonly ISender mediator;
    private string searchString;

    public DialogItemSelectionViewModel(ISender mediator)
    {
        this.mediator = mediator;
        this.AddCommand = new DelegateCommand(this.Add);
    }

    public DelegateCommand AddCommand { get; }

    public string SearchString
    {
        get => this.searchString;
        set
        {
            this.SetProperty(ref this.searchString, value);
            this.ApplySearchFilter();
        }
    }

    public SmartCollection<ItemModel> SourceItems { get; } = new();
    public SmartCollection<ItemModel> FilteredItems { get; } = new();
    public List<ItemModel> SelectedItems { get; } = new();
    public string Title => "Item Selection";

    public bool CanCloseDialog()
    {
        return true;
    }

    public void OnDialogClosed()
    {
    }

    public void OnDialogOpened(IDialogParameters parameters)
    {
    }

    public event Action<IDialogResult> RequestClose;


    private void ApplySearchFilter()
    {
        var filteredItems = this.SourceItems.Where(item => string.IsNullOrWhiteSpace(this.SearchString)
                                                           || (item.ItemName?.Contains(this.SearchString,
                                                               StringComparison.InvariantCultureIgnoreCase) ?? false)
                                                           || item.TemplateId.ToString().Contains(this.SearchString)
                                                           || (item.ToolTip?.Contains(this.SearchString,
                                                               StringComparison.InvariantCultureIgnoreCase) ?? false));
        this.FilteredItems.Reset(filteredItems);
    }

    private void RaiseRequestClose(IDialogResult dialogResult)
    {
        this.RequestClose?.Invoke(dialogResult);
    }

    public void Add()
    {
        var dialogParameters = new DialogParameters
        {
            { SelectedItemsParameter, this.SelectedItems }
        };
        var dialogResult = new DialogResult(ButtonResult.None, dialogParameters);
        this.RaiseRequestClose(dialogResult);
    }

    public async void Loaded()
    {
        var response = await this.mediator.Send(new GetAllItemsRequest());
        this.SourceItems.Reset(response.Items.Where(item => !string.IsNullOrWhiteSpace(item.ItemName))
            .OrderBy(item => item.ItemName));
        this.FilteredItems.Reset(this.SourceItems);
    }
}