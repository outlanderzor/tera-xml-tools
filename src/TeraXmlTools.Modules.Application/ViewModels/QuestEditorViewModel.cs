﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using MediatR;
using Prism.Commands;
using Prism.Regions;
using Prism.Services.Dialogs;
using TeraXmlTools.Core.Extensions;
using TeraXmlTools.Core.Mvvm;
using TeraXmlTools.Domain.Server.QuestCompensationData;
using TeraXmlTools.Modules.Application.Views;
using TeraXmlTools.Services.Items.GetAllItems;
using TeraXmlTools.Services.Quests.GetAllQuests;
using TeraXmlTools.Services.Quests.UpdateQuestCompensationData;

// ReSharper disable AsyncVoidLambda

namespace TeraXmlTools.Modules.Application.ViewModels;

public class QuestEditorViewModel : RegionViewModelBase
{
    public const string QuestModelParameter = nameof(QuestModelParameter);
    private readonly IDialogService dialogService;
    private readonly ISender mediator;
    private QuestModel model;
    private string statusBarText;

    public QuestEditorViewModel(ISender mediator, IRegionManager regionManager, IDialogService dialogService) :
        base(regionManager)
    {
        this.mediator = mediator;
        this.dialogService = dialogService;
        this.SaveChangesCommand = new DelegateCommand(async () => await this.SaveChangesAsync());
        this.DeleteRewardItemCommand = new DelegateCommand<QuestItemRewardModel>(this.ExecuteMethod);
        this.OpenItemSelectionDialog = new DelegateCommand(this.AddRewardItem);
    }

    public DelegateCommand SaveChangesCommand { get; }
    public DelegateCommand<QuestItemRewardModel> DeleteRewardItemCommand { get; }
    public DelegateCommand OpenItemSelectionDialog { get; }

    public QuestModel Model
    {
        get => this.model;
        set => this.SetProperty(ref this.model, value);
    }

    public string StatusBarText
    {
        get => this.statusBarText;
        set => this.SetProperty(ref this.statusBarText, value);
    }

    private void AddRewardItem()
    {
        this.dialogService.Show(nameof(DialogItemSelectionView), result =>
        {
            var selectedItems =
                result.Parameters.GetValue<List<ItemModel>>(DialogItemSelectionViewModel.SelectedItemsParameter);
            if (selectedItems == null) return;
            this.Model.RewardItems.AddRange(selectedItems.Adapt<List<QuestItemRewardModel>>());
        });
    }

    private void ExecuteMethod(QuestItemRewardModel item)
    {
        if (item == null)
            return;
        this.Model.RewardItems.Remove(item);
    }

    private UpdateQuestCompensationDataModel CreateUpdateModel()
    {
        var updateModel = new UpdateQuestCompensationDataModel
        {
            QuestId = this.Model.QuestId,
            QuestExp = this.Model.QuestExp,
            QuestGold = this.Model.QuestGold,
            RewardItems = this.Model.RewardItems.Select(item => new Item
            {
                TemplateId = item.TemplateId,
                Class = item.Class,
                Quantity = item.Quantity
            })
            .ToList()
        };
        return updateModel;
    }

    private async Task SaveChangesAsync()
    {
        try
        {
            var updateModel = this.CreateUpdateModel();
            await this.mediator.Send(new UpdateQuestCompensationDataRequest(updateModel));
            this.StatusBarText = $"{DateTime.Now}: Quest {updateModel.QuestId} updated.";
        }
        catch (Exception ex)
        {
            this.dialogService.HandleError(ex);
        }
    }

    public override void OnNavigatedTo(NavigationContext navigationContext)
    {
        navigationContext.Parameters.TryGetValue(nameof(QuestModelParameter), out QuestModel questModel);
        this.Model = questModel;
        this.StatusBarText = $"{DateTime.Now}: Quest {questModel.QuestId} loaded.";
    }
}