﻿using Mapster;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TeraXmlTools.Core;
using TeraXmlTools.Modules.Application.Designtime;
using TeraXmlTools.Modules.Application.Views;

namespace TeraXmlTools.Modules.Application;

public class ApplicationModule : IModule
{
    private readonly IRegionManager regionManager;

    public ApplicationModule(IRegionManager regionManager)
    {
        this.regionManager = regionManager;
    }

    public void OnInitialized(IContainerProvider containerProvider)
    {
        //this.regionManager.RequestNavigate(RegionNames.MainContentRegion, nameof(QuestSelectionView));
    }

    public void RegisterTypes(IContainerRegistry containerRegistry)
    {
        TypeAdapterConfig.GlobalSettings.Default.PreserveReference(true);
        containerRegistry.RegisterForNavigation<ViewA>();
        containerRegistry.RegisterForNavigation<QuestEditorView>();
        containerRegistry.RegisterForNavigation<QuestSelectionView>();
        containerRegistry.RegisterDialog<DialogItemSelectionView>();
    }
}