﻿using TeraXmlTools.Common.Collections;
using TeraXmlTools.Services.Items.GetAllItems;

namespace TeraXmlTools.Modules.Application.Designtime;

public class DialogItemSelectionViewModel : ViewModels.DialogItemSelectionViewModel
{
    public DialogItemSelectionViewModel()
        : base(null)
    {
        this.SourceItems.Add(new ItemModel{TemplateId = 123, ItemName = "Kaia`s Sword", ToolTip = "This is a sword."});
        this.FilteredItems.AddRange(this.SourceItems);
        this.SelectedItems.AddRange(this.SourceItems);
    }
}