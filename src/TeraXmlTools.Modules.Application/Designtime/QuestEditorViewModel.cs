﻿using System.Collections.Generic;
using TeraXmlTools.Common.Collections;
using TeraXmlTools.Services.Quests.GetAllQuests;

namespace TeraXmlTools.Modules.Application.Designtime;

public class QuestEditorViewModel : ViewModels.QuestEditorViewModel
{
    public QuestEditorViewModel() : base(null, null, null)
    {
        this.Model = new QuestModel
        {
            RewardItems = new SmartCollection<QuestItemRewardModel>
            {
                new()
                {
                    Class = "sorcerer", ItemName = "Apple", Quantity = 2, TemplateId = 123, ToolTip = "Some tooltip."
                }
            }
        };
    }
}