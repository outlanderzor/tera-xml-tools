﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace TeraXmlTools.Modules.Application.Designtime
{
    public class QuestSelectionViewModel : ViewModels.QuestSelectionViewModel
    {
        public QuestSelectionViewModel() : base(null, null)
        {
        }
    }
}
