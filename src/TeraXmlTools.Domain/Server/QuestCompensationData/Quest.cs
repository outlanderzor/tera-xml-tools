﻿using System.Collections.Generic;
using System.Xml.Serialization;
using QuestContants = TeraXmlTools.Domain.DomainConstants.QuestCompensationData.Quest;

namespace TeraXmlTools.Domain.Server.QuestCompensationData;

[XmlRoot(ElementName = QuestContants.ElementName)]
public sealed record Quest
{
    [XmlElement(ElementName = QuestContants.Compensation.ElementName)]
    public List<Compensation> Compensations { get; set; } = new();

    [XmlAttribute(AttributeName = QuestContants.QuestId)]
    public int QuestId { get; set; }
}