﻿using System.Collections.Generic;
using System.Xml.Serialization;
using QuestCompensationDataConstants = TeraXmlTools.Domain.DomainConstants.QuestCompensationData;

namespace TeraXmlTools.Domain.Server.QuestCompensationData;

[XmlRoot(ElementName = QuestCompensationDataConstants.ElementName)]
public sealed record QuestCompensationData : RootModel
{
    [XmlElement(ElementName = QuestCompensationDataConstants.Quest.ElementName)]
    public List<Quest> Quests { get; set; } = new();

    [XmlAttribute(AttributeName = QuestCompensationDataConstants.Quest.HuntingZoneId)]
    public int HuntingZoneId { get; set; }
}