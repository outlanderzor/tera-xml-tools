﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using CompensationTypeConstants =
    TeraXmlTools.Domain.DomainConstants.QuestCompensationData.Quest.Compensation.CompensationType;

namespace TeraXmlTools.Domain.Server.QuestCompensationData;

[XmlRoot(ElementName = CompensationTypeConstants.ElementName)]
public sealed record CompensationType
{
    [XmlElement(ElementName = CompensationTypeConstants.Item.ElementName)]
    public List<Item> Items { get; set; } = new();

    [XmlAttribute(AttributeName = CompensationTypeConstants.Type)]
    public string Type { get; set; } = "normal";

    [XmlAttribute(AttributeName = CompensationTypeConstants.Exp)]
    public string? Exp { get; set; }

    [XmlAttribute(AttributeName = CompensationTypeConstants.Gold)]
    public string? Gold { get; set; }

    [XmlAttribute(AttributeName = CompensationTypeConstants.ItemBag)]
    public string? ItemBag { get; set; }

    [XmlAttribute(AttributeName = CompensationTypeConstants.Memo)]
    public string? Memo { get; set; }

    [XmlAttribute(AttributeName = CompensationTypeConstants.PolicyPoint)]
    public string? PolicyPoint { get; set; }

    [XmlAttribute(AttributeName = CompensationTypeConstants.NpcGuildId)]
    public string? NpcGuildId { get; set; }

    [XmlAttribute(AttributeName = CompensationTypeConstants.ReputationExp)]
    public string? ReputationExp { get; set; }

    [XmlAttribute(AttributeName = CompensationTypeConstants.ReputationPoint)]
    public string? ReputationPoint { get; set; }
}