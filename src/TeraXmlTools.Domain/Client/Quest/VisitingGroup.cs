﻿using System.Collections.Generic;
using System.Xml.Serialization;
using VisitingGroupContants = TeraXmlTools.Domain.DomainConstants.Quest.QuestTasks.QuestTask.Body.VisitingGroup;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = VisitingGroupContants.ElementName)]
public sealed record VisitingGroup
{
    [XmlElement(ElementName = VisitingGroupContants.Groups)]
    public List<VisitingGroup> Groups { get; set; } = new();

    [XmlElement(ElementName = VisitingGroupContants.NpcId)]
    public string? NpcId { get; set; }
}