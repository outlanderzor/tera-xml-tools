﻿using System.Xml.Serialization;
using MonsterDesignationContants = TeraXmlTools.Domain.DomainConstants.Quest.QuestTasks.QuestTask.Body.MonsterDesignation;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = MonsterDesignationContants.ElementName)]
public sealed record MonsterDesignation
{
    [XmlElement(ElementName = MonsterDesignationContants.MonsterDesignationMonsterId.ElementName)]
    public MonsterDesignationMonsterId? MonsterDesignationMonsterId { get; set; }
}