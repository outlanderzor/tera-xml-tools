﻿using System.Xml.Serialization;
using QuestConstants = TeraXmlTools.Domain.DomainConstants.Quest;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = QuestConstants.ElementName, Namespace = QuestConstants.Namespace)]
public sealed record Quest : RootModel
{
    [XmlElement(ElementName = QuestConstants.Header.ElementName)]
    public Header? Header { get; set; }

    [XmlElement(ElementName = QuestConstants.QuestTasks.ElementName)]
    public QuestTasks? QuestTasks { get; set; }

    [XmlAttribute(AttributeName = QuestConstants.Id)]
    public int Id { get; set; }

    [XmlText]
    public string? Text { get; set; }
}