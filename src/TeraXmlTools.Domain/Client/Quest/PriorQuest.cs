﻿using System.Xml.Serialization;
using PriorQuestConstants = TeraXmlTools.Domain.DomainConstants.Quest.Header.PerformanceCondition.PriorQuest;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = PriorQuestConstants.ElementName)]
public sealed record PriorQuest
{
    [XmlElement(ElementName = PriorQuestConstants.PriorQuestId.ElementName)]
    public PriorQuestId? PriorQuestId { get; set; }
}