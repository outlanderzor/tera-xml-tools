﻿using System.Xml.Serialization;
using PerformanceConditionContants = TeraXmlTools.Domain.DomainConstants.Quest.Header.PerformanceCondition;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = PerformanceConditionContants.ElementName)]
public sealed record PerformanceCondition
{
    [XmlElement(ElementName = PerformanceConditionContants.GuildLevel)]
    public string? GuildLevel { get; set; }

    [XmlElement(ElementName = PerformanceConditionContants.PriorQuest.ElementName)]
    public PriorQuest? PriorQuest { get; set; }

    [XmlElement(ElementName = PerformanceConditionContants.LordGuild)]
    public string? LordGuild { get; set; }

    [XmlElement(ElementName = PerformanceConditionContants.MinimumLevel)]
    public int MinimumLevel { get; set; }
}