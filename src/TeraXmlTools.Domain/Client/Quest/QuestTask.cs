﻿using System.Xml.Serialization;
using QuestTaskConstants = TeraXmlTools.Domain.DomainConstants.Quest.QuestTasks.QuestTask;
using HeaderConstants = TeraXmlTools.Domain.DomainConstants.Quest.Header;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = QuestTaskConstants.ElementName)]
public sealed record QuestTask
{
    [XmlElement(ElementName = QuestTaskConstants.Body.ElementName)]
    public Body? Body { get; set; }

    [XmlElement(ElementName = HeaderConstants.ElementName)]
    public Header? Header { get; set; }

    [XmlAttribute(AttributeName = QuestTaskConstants.Id)]
    public int Id { get; set; }

    [XmlText]
    public string? Text { get; set; }
}