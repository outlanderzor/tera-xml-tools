﻿using System.Xml.Serialization;
using ProgressConditionConstants = TeraXmlTools.Domain.DomainConstants.Quest.QuestTasks.QuestTask.Body.ProgressCondition;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = ProgressConditionConstants.ElementName)]
public sealed record ProgressCondition
{
    [XmlElement(ElementName = ProgressConditionConstants.NumberOfHunters)]
    public string? NumberOfHunters { get; set; }

    [XmlElement(ElementName = ProgressConditionConstants.TimeLimit)]
    public string? TimeLimit { get; set; }
}