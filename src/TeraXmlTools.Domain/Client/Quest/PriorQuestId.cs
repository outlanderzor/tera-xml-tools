﻿using System.Xml.Serialization;
using PriorQuestIdConstants = TeraXmlTools.Domain.DomainConstants.Quest.Header.PerformanceCondition.PriorQuest.PriorQuestId;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = PriorQuestIdConstants.ElementName)]
public sealed record PriorQuestId
{
    [XmlElement(ElementName = PriorQuestIdConstants.QuestId)]
    public string? QuestId { get; set; }
}