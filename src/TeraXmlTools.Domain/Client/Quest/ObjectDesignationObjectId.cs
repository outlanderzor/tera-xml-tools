﻿using System.Xml.Serialization;
using ObjectDesignationObjectIdConstants = TeraXmlTools.Domain.DomainConstants.Quest.QuestTasks.QuestTask.Body.ObjectDesignation.ObjectDesignationObjectId;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = ObjectDesignationObjectIdConstants.ElementName)]
public sealed record ObjectDesignationObjectId
{
    [XmlElement(ElementName = ObjectDesignationObjectIdConstants.NumberOfOperations)]
    public int NumberOfOperations { get; set; }

    [XmlElement(ElementName = ObjectDesignationObjectIdConstants.ObjectId)]
    public int ObjectId { get; set; }
}