﻿using System.Xml.Serialization;
using MonsterDesignationMonsterIdContants = TeraXmlTools.Domain.DomainConstants.Quest.QuestTasks.QuestTask.Body.MonsterDesignation.MonsterDesignationMonsterId;


namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = MonsterDesignationMonsterIdContants.ElementName)]
public sealed record MonsterDesignationMonsterId
{
    [XmlElement(ElementName = MonsterDesignationMonsterIdContants.MonsterId)]
    public string? MonsterId { get; set; }

    [XmlElement(ElementName = MonsterDesignationMonsterIdContants.NumberOfHunts)]
    public string? NumberOfHunts { get; set; }
}