﻿using System.Collections.Generic;
using System.Xml.Serialization;
using QuestTasksContants = TeraXmlTools.Domain.DomainConstants.Quest.QuestTasks;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = QuestTasksContants.ElementName)]
public sealed record QuestTasks
{
    [XmlElement(ElementName = QuestTasksContants.Task)]
    public List<QuestTask> Tasks { get; set; } = new();
}