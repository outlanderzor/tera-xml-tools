﻿using System.Xml.Serialization;
using OcorrenceConditionConstants = TeraXmlTools.Domain.DomainConstants.Quest.Header.OccurrenceCondition;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = OcorrenceConditionConstants.ElementName)]
public sealed record OccurrenceCondition
{
    [XmlElement(ElementName = OcorrenceConditionConstants.OrderImmediately)]
    public int OrderImmediately { get; set; }
}