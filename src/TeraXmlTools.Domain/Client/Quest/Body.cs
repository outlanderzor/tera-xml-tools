﻿using System.Xml.Serialization;
using BodyConstants = TeraXmlTools.Domain.DomainConstants.Quest.QuestTasks.QuestTask.Body;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = BodyConstants.ElementName)]
public sealed record Body
{
    [XmlElement(ElementName = BodyConstants.VisitingGroup.ElementName)]
    public VisitingGroup? VisitingGroup { get; set; }

    [XmlElement(ElementName = BodyConstants.Compensation)]
    public string? Compensation { get; set; }

    [XmlElement(ElementName = BodyConstants.JournalText)]
    public string? JournalText { get; set; }

    [XmlElement(ElementName = BodyConstants.MonsterDesignation.ElementName)]
    public MonsterDesignation? MonsterDesignation { get; set; }

    [XmlElement(ElementName = BodyConstants.ProgressCondition.ElementName)]
    public ProgressCondition? ProgressCondition { get; set; }

    [XmlElement(ElementName = BodyConstants.TargetArea)]
    public string? TargetArea { get; set; }

    [XmlElement(ElementName = BodyConstants.TimeLimit)]
    public string? TimeLimit { get; set; }

    [XmlElement(ElementName = BodyConstants.LocalPeople)]
    public string? LocalPeople { get; set; }

    [XmlElement(ElementName = BodyConstants.ObjectDesignation.ElementName)]
    public ObjectDesignation? ObjectDesignation { get; set; }

    [XmlElement(ElementName = BodyConstants.TargetNpcDesignation)]
    public string? TargetNpcDesignation { get; set; }

    [XmlElement(ElementName = BodyConstants.NumberOfEscorts)]
    public int NumberOfEscorts { get; set; }
}