﻿using System.Xml.Serialization;
using HeaderConstants = TeraXmlTools.Domain.DomainConstants.Quest.Header;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = HeaderConstants.ElementName)]
public sealed record Header
{
    [XmlElement(ElementName = HeaderConstants.QuestNumber)]
    public string? QuestNumber { get; set; }

    [XmlElement(ElementName = HeaderConstants.QuestTitle)]
    public string? QuestTitle { get; set; }

    [XmlElement(ElementName = HeaderConstants.RepeatQuest)]
    public string? RepeatQuest { get; set; }

    [XmlElement(ElementName = HeaderConstants.OccurrenceCondition.ElementName)]
    public OccurrenceCondition? OccurrenceCondition { get; set; }

    [XmlElement(ElementName = HeaderConstants.PerformanceCondition.ElementName)]
    public PerformanceCondition? PerformanceCondition { get; set; }

    [XmlElement(ElementName = HeaderConstants.PracticeArea)]
    public string? PracticeArea { get; set; }

    [XmlElement(ElementName = HeaderConstants.StoryGroupId)]
    public string? StoryGroupId { get; set; }

    [XmlElement(ElementName = HeaderConstants.SummaryInformation)]
    public int SummaryInformation { get; set; }

    [XmlElement(ElementName = HeaderConstants.AppropriatePerformanceLevel)]
    public string? AppropriatePerformanceLevel { get; set; }

    [XmlElement(ElementName = HeaderConstants.AppropriateNumberOfParticipants)]
    public string? AppropriateNumberOfParticipants { get; set; }

    [XmlElement(ElementName = HeaderConstants.MeetinghouseClassification)]
    public string? MeetinghouseClassification { get; set; }

    [XmlElement(ElementName = HeaderConstants.AdditionalCompensation)]
    public string? AdditionalCompensation { get; set; }

    [XmlElement(ElementName = HeaderConstants.QuestType)]
    public string? QuestType { get; set; }

    [XmlElement(ElementName = HeaderConstants.SpecialGuide)]
    public string? SpecialGuide { get; set; }

    [XmlElement(ElementName = HeaderConstants.ContinentId)]
    public string? ContinentId { get; set; }

    [XmlElement(ElementName = HeaderConstants.Id)]
    public int Id { get; set; }

    [XmlElement(ElementName = HeaderConstants.JournalScript)]
    public int JournalScript { get; set; }

    [XmlElement(ElementName = HeaderConstants.Pos)]
    public string? Pos { get; set; }

    [XmlElement(ElementName = HeaderConstants.NextTask)]
    public string? NextTask { get; set; }

    [XmlElement(ElementName = HeaderConstants.Name)]
    public string? Name { get; set; }
}