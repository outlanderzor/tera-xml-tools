﻿using System.Xml.Serialization;

namespace TeraXmlTools.Domain.Client.Quest;

[XmlRoot(ElementName = "오브젝트지정")]
public sealed record ObjectDesignation
{
    [XmlElement(ElementName = "오브젝트지정")]
    public ObjectDesignationObjectId? ObjectDesignationObjectId { get; set; }
}