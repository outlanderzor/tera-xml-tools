﻿using System.Collections.Generic;
using System.Xml.Serialization;
using StrSheetConstants = TeraXmlTools.Domain.DomainConstants.StrSheet;

namespace TeraXmlTools.Domain.Client.StrSheet;

[XmlRoot(ElementName = StrSheetConstants.WorkObject.ElementName, Namespace = StrSheetConstants.WorkObject.Namespace)]
public sealed record StrSheetWorkObject : RootModel, IStrSheet
{
    [XmlElement(ElementName = StrSheetConstants.String.ElementName)]
    public List<StrSheetString> String { get; set; } = new();
}