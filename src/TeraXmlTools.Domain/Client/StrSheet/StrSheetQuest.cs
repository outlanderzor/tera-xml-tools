﻿using System.Collections.Generic;
using System.Xml.Serialization;
using StrSheetConstants = TeraXmlTools.Domain.DomainConstants.StrSheet;

namespace TeraXmlTools.Domain.Client.StrSheet;

[XmlRoot(ElementName = StrSheetConstants.Quest.ElementName, Namespace = StrSheetConstants.Quest.Namespace)]
public sealed record StrSheetQuest : RootModel, IStrSheet
{
    [XmlElement(ElementName = StrSheetConstants.String.ElementName)]
    public List<StrSheetString> String { get; set; } = new();
}