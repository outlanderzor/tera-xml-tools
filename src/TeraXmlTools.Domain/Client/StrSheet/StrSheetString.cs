﻿using System.Xml.Serialization;
using StringConstants = TeraXmlTools.Domain.DomainConstants.StrSheet.String;

namespace TeraXmlTools.Domain.Client.StrSheet;

[XmlRoot(ElementName = StringConstants.ElementName)]
public sealed record StrSheetString
{
    [XmlAttribute(AttributeName = StringConstants.Id)]
    public int Id { get; set; }

    [XmlAttribute(AttributeName = StringConstants.StringProperty)]
    public string? String { get; set; }

    [XmlAttribute(AttributeName = StringConstants.ReadableId)]
    public string? ReadableId { get; set; }

    [XmlAttribute(AttributeName = StringConstants.ToolTip)]
    public string? ToolTip { get; set; }

    [XmlText]
    public string? Text { get; set; }
}