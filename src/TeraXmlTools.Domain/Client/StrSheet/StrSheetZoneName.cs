﻿using System.Collections.Generic;
using System.Xml.Serialization;
using StrSheetConstants = TeraXmlTools.Domain.DomainConstants.StrSheet;

namespace TeraXmlTools.Domain.Client.StrSheet;

[XmlRoot(ElementName = StrSheetConstants.ZoneName.ElementName, Namespace = StrSheetConstants.ZoneName.Namespace)]
public sealed record StrSheetZoneName : RootModel, IStrSheet
{
    [XmlElement(ElementName = StrSheetConstants.String.ElementName)]
    public List<StrSheetString> String { get; set; } = new();
}