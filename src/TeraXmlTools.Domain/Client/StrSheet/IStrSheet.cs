﻿using System.Collections.Generic;

namespace TeraXmlTools.Domain.Client.StrSheet;

public interface IStrSheet
{
    List<StrSheetString> String { get; set; }
}