﻿using System.Collections.Generic;
using System.Xml.Serialization;
using StrSheetConstants = TeraXmlTools.Domain.DomainConstants.StrSheet;

namespace TeraXmlTools.Domain.Client.StrSheet;

[XmlRoot(ElementName = StrSheetConstants.Item.ElementName, Namespace = StrSheetConstants.Item.Namespace)]
public sealed record StrSheetItem : RootModel, IStrSheet
{
    [XmlElement(ElementName = StrSheetConstants.String.ElementName)]
    public List<StrSheetString> String { get; set; } = new();
}