﻿using System.Xml.Serialization;
using ItemConstants =
    TeraXmlTools.Domain.DomainConstants.QuestCompensationData.Quest.Compensation.CompensationType.Item;

namespace TeraXmlTools.Domain.Client.QuestCompensationData;

[XmlRoot(ElementName = ItemConstants.ElementName)]
public sealed record Item
{
    [XmlAttribute(AttributeName = ItemConstants.TemplateId)]
    public int TemplateId { get; set; }

    [XmlAttribute(AttributeName = ItemConstants.Quantity)]
    public int Quantity { get; set; }

    [XmlAttribute(AttributeName = ItemConstants.Class)]
    public string? Class { get; set; }
}