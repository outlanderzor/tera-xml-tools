﻿using System.Collections.Generic;
using System.Xml.Serialization;
using QuestConstants = TeraXmlTools.Domain.DomainConstants.QuestCompensationData.Quest;

namespace TeraXmlTools.Domain.Client.QuestCompensationData;

[XmlRoot(ElementName = QuestConstants.ElementName)]
public sealed record Quest
{
    [XmlElement(ElementName = QuestConstants.Compensation.ElementName)]
    public List<Compensation> Compensations { get; set; } = new();

    [XmlAttribute(AttributeName = QuestConstants.QuestId)]
    public int QuestId { get; set; }
}