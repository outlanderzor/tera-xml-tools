﻿using System.Collections.Generic;
using System.Xml.Serialization;
using QuestCompensationDataConstants = TeraXmlTools.Domain.DomainConstants.QuestCompensationData;

namespace TeraXmlTools.Domain.Client.QuestCompensationData;

[XmlRoot(ElementName = QuestCompensationDataConstants.ElementName,
    Namespace = QuestCompensationDataConstants.Namespace)]
public sealed record QuestCompensationData : RootModel
{
    [XmlElement(ElementName = QuestCompensationDataConstants.Quest.ElementName)]
    public List<Quest> Quests { get; set; } = new();
}