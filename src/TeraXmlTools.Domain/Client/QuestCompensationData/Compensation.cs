﻿using System.Collections.Generic;
using System.Xml.Serialization;
using CompensationConstants = TeraXmlTools.Domain.DomainConstants.QuestCompensationData.Quest.Compensation;

namespace TeraXmlTools.Domain.Client.QuestCompensationData;

[XmlRoot(ElementName = CompensationConstants.ElementName)]
public sealed record Compensation
{
    [XmlElement(ElementName = CompensationConstants.CompensationType.ElementName)]
    public List<CompensationType> CompensationTypes { get; set; } = new();
}