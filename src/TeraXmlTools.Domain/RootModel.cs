﻿using System.Xml.Serialization;

namespace TeraXmlTools.Domain;

public interface IRootModel
{
    string? FileName { get; set; }
    string? SchemaLocation { get; set; }
}

public abstract record RootModel : IRootModel
{
    [XmlIgnore]
    public string? FileName { get; set; }

    [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
    public string? SchemaLocation { get; set; }
}