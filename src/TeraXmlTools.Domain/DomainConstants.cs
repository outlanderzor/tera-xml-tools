﻿// ReSharper disable MemberHidesStaticFromOuterClass
namespace TeraXmlTools.Domain;

/// <summary>
/// Keeps the XML domain model metadata.
/// Mostly useful to improve the readability of some files that use korean tags
/// and the usage of XQuery.
/// </summary>
public static class DomainConstants
{
    public static class QuestCompensationData
    {
        public const string ElementName = "QuestCompensationData";
        public const string Namespace = "https://vezel.dev/novadrop/dc/QuestCompensationData";
        public static class Quest
        {
            public const string ElementName = "Quest";
            public const string QuestId = "questId";
            public const string HuntingZoneId = "huntingZoneId";
            public static class Compensation
            {
                public const string ElementName = "Compensation";
                public const string CompensationId = "compensationId";
                public static class CompensationType
                {
                    public const string ElementName = "CompensationType";
                    public const string Type = "type";
                    public const string ItemBag = "itemBag";
                    public const string Exp = "exp";
                    public const string Gold = "gold";
                    public const string Memo = "memo";
                    public const string PolicyPoint = "policyPoint";
                    public const string NpcGuildId  = "npcGuildId";
                    public const string ReputationExp = "reputationExp";
                    public const string ReputationPoint = "reputationPoint";
                    public static class Item
                    {
                        public const string ElementName = "Item";
                        public const string TemplateId = "templateId";
                        public const string Quantity = "quantity";
                        public const string Class = "class";
                    }
                }
            }
        }
    }
    public static class Quest
    {
        public const string ElementName = "Quest";
        public const string Namespace = "https://vezel.dev/novadrop/dc/Quest";
        public const string Id = "id";
        public static class Header
        {
            public const string ElementName = "Header";
            public const string QuestNumber = "Quest번호";
            public const string QuestTitle = "Quest제목";
            public const string RepeatQuest = "반복퀘스트";
            public static class OccurrenceCondition
            {
                public const string ElementName = "발생조건";
                public const string OrderImmediately = "즉시수주";
            }
            public static class PerformanceCondition
            {
                public const string ElementName = "수행조건";
                public const string GuildLevel = "길드레벨";
                public static class PriorQuest
                {
                    public const string ElementName = "선행퀘스트";
                    public static class PriorQuestId
                    {
                        public const string ElementName = "선행퀘스트";
                        public const string QuestId = "퀘스트Id";
                    }
                }
                public const string LordGuild = "영주길드";
                public const string MinimumLevel = "최소레벨";
            }
            public const string PracticeArea = "수행지역";
            public const string StoryGroupId = "스토리그룹Id";
            public const string SummaryInformation = "요약정보";
            public const string AppropriatePerformanceLevel = "적정수행레벨";
            public const string AppropriateNumberOfParticipants = "적정수행인원";
            public const string MeetinghouseClassification = "집회소분류";
            public const string AdditionalCompensation = "추가보상";
            public const string QuestType = "퀘스트종류";
            public const string SpecialGuide = "특수가이드";
            public const string ContinentId = "ContinentId";
            public const string Id = "Id";
            public const string JournalScript = "JournalScript";
            public const string Pos = "Pos";
            public const string NextTask = "다음Task";
            public const string Name = "이름";
        }
        public static class QuestTasks
        {
            public const string ElementName = "Tasks";
            public const string Task = "Task";
            public static class QuestTask
            {
                public const string ElementName = "Task";
                public const string Id = "id";
                public static class Body
                {
                    public const string ElementName = "Body";
                    public static class VisitingGroup
                    {
                        public const string ElementName = "방문그룹";
                        public const string Groups = "방문그룹";
                        public const string NpcId = "NPCId";
                    }
                    public const string Compensation = "보상";
                    public const string JournalText = "저널Text";
                    public static class MonsterDesignation
                    {
                        public const string ElementName = "몬스터지정";
                        public static class MonsterDesignationMonsterId
                        {
                            public const string ElementName = "몬스터지정";
                            public const string MonsterId = "몬스터Id";
                            public const string NumberOfHunts = "사냥마리수";
                        }
                    }
                    public static class ProgressCondition
                    {
                        public const string ElementName = "진행조건";
                        public const string NumberOfHunters = "사냥인원";
                        public const string TimeLimit = "제한시간";
                    }
                    public const string TargetArea = "목표지역";
                    public const string TimeLimit = "제한시간";
                    public const string LocalPeople = "지역명";
                    public static class ObjectDesignation
                    {
                        public const string ElementName = "오브젝트지정";
                        public static class ObjectDesignationObjectId
                        {
                            public const string ElementName = "오브젝트지정";
                            public const string NumberOfOperations = "동작횟수";
                            public const string ObjectId = "오브젝트Id";
                        }
                    }
                    public const string TargetNpcDesignation = "대상NPC지정";
                    public const string NumberOfEscorts = "호위횟수";
                }
            }
        }
    }
    public static class StrSheet
    {
        public static class String
        {
            public const string ElementName = "String";

            public const string Id = "id";
            public const string StringProperty = "string";
            public const string ReadableId = "readableId";
            public const string ToolTip = "toolTip";
        }

        public static class Item
        {
            public const string ElementName = "StrSheet_Item";
            public const string Namespace = "https://vezel.dev/novadrop/dc/StrSheet_Item";
        }
        public static class Quest
        {
            public const string ElementName = "StrSheet_Quest";
            public const string Namespace = "https://vezel.dev/novadrop/dc/StrSheet_Quest";
        }
        public static class WorkObject
        {
            public const string ElementName = "StrSheet_WorkObject";
            public const string Namespace = "https://vezel.dev/novadrop/dc/StrSheet_WorkObject";
        }
        public static class ZoneName
        {
            public const string ElementName = "StrSheet_ZoneName";
            public const string Namespace = "https://vezel.dev/novadrop/dc/StrSheet_ZoneName";
        }

    }
}