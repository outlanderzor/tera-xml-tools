﻿using MediatR;
using TeraXmlTools.Common.Repositories;
using Client = TeraXmlTools.Domain.Client.QuestCompensationData;
using Server = TeraXmlTools.Domain.Server.QuestCompensationData;

namespace TeraXmlTools.Services.Quests.UpdateQuestCompensationData;

public sealed record UpdateQuestCompensationDataModel
{
    public int QuestId { get; init; } 
    public long? QuestExp { get; init; } 
    public long? QuestGold { get; init; }
    public List<Server.Item> RewardItems { get; init; } = new();
}

public sealed record UpdateQuestCompensationDataRequest(UpdateQuestCompensationDataModel Model) : IRequest<Unit>
{
    internal sealed class UpdateQuestCompensationDataRequestHandler : IRequestHandler<UpdateQuestCompensationDataRequest, Unit>
    {
        private readonly IUnitOfWorkFactory uowFactory;

        public UpdateQuestCompensationDataRequestHandler(IUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }

        public async Task<Unit> Handle(UpdateQuestCompensationDataRequest request, CancellationToken cancellationToken)
        {
            await using var uow = this.uowFactory.CreateUnitOfWork();
            await uow.ClientQuestCompensationData
                .UpdateQuestCompensationData<Client.QuestCompensationData>(request.Model.QuestId, request.Model.QuestExp, request.Model.QuestGold, request.Model.RewardItems);
            await uow.ServerQuestCompensationData
                .UpdateQuestCompensationData<Server.QuestCompensationData>(request.Model.QuestId, request.Model.QuestExp, request.Model.QuestGold, request.Model.RewardItems);
            await uow.SaveChangesAsync();
            return Unit.Value;
        }
    }
}