﻿using MediatR;
using TeraXmlTools.Common.Collections;
using TeraXmlTools.Common.Extensions;
using TeraXmlTools.Common.Repositories;
using TeraXmlTools.Domain.Server.QuestCompensationData;

namespace TeraXmlTools.Services.Quests.GetAllQuests;

public sealed record GetAllQuestsRequest : IRequest<GetAllQuestsResponse>
{
    internal sealed class GetAllQuestsRequestHandler : IRequestHandler<GetAllQuestsRequest, GetAllQuestsResponse>
    {
        private readonly IUnitOfWorkFactory uowFactory;

        public GetAllQuestsRequestHandler(IUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }

        public async Task<GetAllQuestsResponse> Handle(GetAllQuestsRequest request, CancellationToken cancellationToken)
        {
            await using var uow = this.uowFactory.CreateUnitOfWork();
            var getQuestStringsTask = uow.ClientStrSheet.GetQuestStrings();
            var getZoneNameStringsTask = uow.ClientStrSheet.GetZoneNameStrings();
            var getQuestCompensationDataTask = uow.ServerQuestCompensationData.GetQuestCompensationData();
            var getQuestsTask = uow.ClientQuest.GetQuests();
            var getItemStringsTask = uow.ClientStrSheet.GetItemStrings();

            await Task.WhenAll(getQuestStringsTask, getZoneNameStringsTask, getQuestCompensationDataTask, getQuestsTask, getItemStringsTask);

            var compensationData = await getQuestCompensationDataTask;
            var queryCompensationData = compensationData
                .SelectMany(cd => cd.Quests
                    .Select(q =>
                        new
                        {
                            q.QuestId,
                            cd.HuntingZoneId,
                            q.Compensations.FirstOrDefault()?.CompensationTypes.FirstOrDefault()?.Exp,
                            q.Compensations.FirstOrDefault()?.CompensationTypes.FirstOrDefault()?.Gold,
                            FileSource = cd.FileName!,
                            Items = q.Compensations.FirstOrDefault()?.CompensationTypes.FirstOrDefault()?.Items
                                .Select(i => i)
                                .ToArray() ?? Array.Empty<Item>()
                        }));

            var quests = await getQuestsTask;
            var questDictionary = quests
                .Select(q => new
                {
                    q.Id,
                    q.Header?.PerformanceCondition?.MinimumLevel,
                    PriorQuestId = q.Header?.PerformanceCondition?.PriorQuest?.PriorQuestId?.QuestId,
                    q.Header?.AppropriatePerformanceLevel
                })
                .ToDictionary(q => q.Id, q => q);

            var itemsStrings = await getItemStringsTask;
            var itemsStringsDictionary = itemsStrings.ToDictionary(i => i.Id, i => new { ItemName = i.String, i.ToolTip });

            var questStringsDictionary = (await getQuestStringsTask).ToDictionary();
            var zoneNameStringsDictionary = (await getZoneNameStringsTask).ToDictionary();
            var result = new List<QuestModel>();
            foreach (var quest in queryCompensationData)
            {
                questStringsDictionary.TryGetValue(int.Parse(quest.QuestId + "001"), out var questName);
                zoneNameStringsDictionary.TryGetValue(quest.HuntingZoneId, out var zoneName);
                questDictionary.TryGetValue(quest.QuestId, out var questData);

                var items = new List<QuestItemRewardModel>();
                foreach (var questItem in quest.Items)
                {
                    itemsStringsDictionary.TryGetValue(questItem.TemplateId, out var itemStrings);
                    items.Add(
                        new QuestItemRewardModel
                        {
                            TemplateId = questItem.TemplateId,
                            Quantity = questItem.Quantity,
                            ItemName = itemStrings?.ItemName,
                            ToolTip = itemStrings?.ToolTip,
                            Class = questItem.Class,
                        });
                }

                result.Add(new QuestModel
                {
                    QuestId = quest.QuestId,
                    QuestName = questName,
                    HuntingZoneId = quest.HuntingZoneId,
                    ZoneName = zoneName,
                    QuestExp = !string.IsNullOrWhiteSpace(quest.Exp) ? (long?)long.Parse(quest.Exp) : default,
                    QuestGold = !string.IsNullOrWhiteSpace(quest.Gold) ? (long?)long.Parse(quest.Gold) : default,
                    MinimumLevel = questData?.MinimumLevel,
                    RecommendedLevel = !string.IsNullOrWhiteSpace(questData?.AppropriatePerformanceLevel) ? (int?)int.Parse(questData.AppropriatePerformanceLevel.GetNumbers().Replace(",", "")) : default,
                    RequiredQuestId = !string.IsNullOrWhiteSpace(questData?.PriorQuestId) ? (int?)int.Parse(questData.PriorQuestId.Replace(",", "")) : default,
                    FileName = quest.FileSource,
                    RewardItems = new SmartCollection<QuestItemRewardModel>(items)
                });
            }

            return new GetAllQuestsResponse(result);
        }
    }
}