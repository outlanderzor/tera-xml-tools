﻿using TeraXmlTools.Common.Collections;

namespace TeraXmlTools.Services.Quests.GetAllQuests;
public sealed class QuestItemRewardModel
{
    public int TemplateId { get; set; }
    public int Quantity { get; set; } = 1;
    public string? ItemName { get; set; }
    public string? ToolTip { get; set; }
    public string? Class { get; set; }
}

public sealed class QuestModel
{
    public int QuestId { get; set; }
    public string? QuestName { get; set; }
    public int HuntingZoneId { get; set; }
    public string? ZoneName { get; set; }
    public long? QuestExp { get; set; }
    public long? QuestGold { get; set; }
    public string FileName { get; set; } = null!;
    public int? MinimumLevel { get; set; }
    public int? RequiredQuestId { get; set; }
    public int? RecommendedLevel { get; set; }

    public SmartCollection<QuestItemRewardModel> RewardItems { get; set; } = null!;
}

public sealed record GetAllQuestsResponse(List<QuestModel> Quests);