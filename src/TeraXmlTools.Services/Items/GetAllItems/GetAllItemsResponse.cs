﻿namespace TeraXmlTools.Services.Items.GetAllItems;
public sealed record ItemModel
{
    public int TemplateId { get; set; }
    public string? ItemName { get; set; }
    public string? ToolTip { get; init; }
}
public sealed record GetAllItemsResponse
{
    public List<ItemModel> Items { get; set; } = null!;
}