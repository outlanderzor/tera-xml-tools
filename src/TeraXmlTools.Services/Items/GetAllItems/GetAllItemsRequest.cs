﻿using MediatR;
using TeraXmlTools.Common.Repositories;

namespace TeraXmlTools.Services.Items.GetAllItems;

public sealed record GetAllItemsRequest : IRequest<GetAllItemsResponse>
{
    internal sealed class GetAllItemsRequestHandler : IRequestHandler<GetAllItemsRequest, GetAllItemsResponse>
    {
        private readonly IUnitOfWorkFactory uowFactory;

        public GetAllItemsRequestHandler(IUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }

        public async Task<GetAllItemsResponse> Handle(GetAllItemsRequest request, CancellationToken cancellationToken)
        {
            await using var uow = this.uowFactory.CreateUnitOfWork();
            var items = await uow.ClientStrSheet.GetItemStrings();
            return new GetAllItemsResponse
            {
                Items = items.Select(item => new ItemModel
                {
                    TemplateId = item.Id,
                    ItemName = item.String,
                    ToolTip = item.ToolTip
                }).ToList()
            };
        }
    }
}