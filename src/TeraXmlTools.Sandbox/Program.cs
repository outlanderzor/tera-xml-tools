﻿// See https://aka.ms/new-console-template for more information

using System.Xml;
using System.Xml.Serialization;
using TeraXmlTools.Domain.Server.QuestCompensationData;

Console.WriteLine("Hello, World!");

const string xml = @"<QuestCompensationData huntingZoneId=""2"">
  <Quest questId=""272"">
    <Compensation compensationId=""1"">
      <CompensationType type=""normal"" itemBag=""allpay"">
        <Item templateId=""219006"" quantity=""1"" />
		<Item templateId=""202144"" quantity=""1"" />
      </CompensationType>
	  <CompensationType type=""normal"" itemBag=""allpay"">
        <Item templateId=""219006"" quantity=""1"" />
		<Item templateId=""202144"" quantity=""1"" />
      </CompensationType>
    </Compensation>
	<Compensation compensationId=""2"">
      <CompensationType type=""normal"" itemBag=""allpay"">
        <Item templateId=""202150"" quantity=""1"" />
		<Item templateId=""219013"" quantity=""1"" />
      </CompensationType>
    </Compensation>
  </Quest>
  <Quest questId=""273"">
	<Compensation compensationId=""1"">
      <CompensationType type=""normal"" itemBag=""allpay"">
        <Item templateId=""202150"" quantity=""1"" />
		<Item templateId=""219013"" quantity=""1"" />
      </CompensationType>
    </Compensation>
  </Quest>
  <Quest questId=""280""/>
  <Quest questId=""277""/>
</QuestCompensationData>";

var sr = new StringReader(xml);
var xmlSerializer = new XmlSerializer(typeof(QuestCompensationData));
var compensationData = (QuestCompensationData?)xmlSerializer.Deserialize(sr)!;
var quest = compensationData.Quests.First(q => q.QuestId == 280);
quest.Compensations = new List<Compensation>
{
    new() { CompensationId = 1, CompensationTypes = new List<CompensationType>
    {
        new() { Exp = "999", Gold = "999" }
    }}
};

var sw = new StringWriter();
var xmlw = XmlWriter.Create(sw, new XmlWriterSettings
{
    OmitXmlDeclaration = true,
    Indent = true
});
xmlSerializer.Serialize(xmlw, compensationData);

Console.Write(sw.ToString());

//var path =
//    "D:\\dev\\tera\\tera-server-client-data\\100.02\\DC\\EUR\\QuestCompensationData\\QuestCompensationData-99.xml";

//var fs = File.Open(path, FileMode.Open);
//var data = SerializationHelper.Deserialize<QuestCompensationData>(fs);

//var tempPath = Path.GetTempFileName();
//var tempFs = File.Open(tempPath, FileMode.Create);
//SerializationHelper.Serialize(tempFs, typeof(QuestCompensationData), data);
//tempFs.Close();

//var fileText = File.ReadAllText(tempPath);

//Console.Write(fileText);
//Console.ReadKey();


//var files = Directory.GetFiles("D:\\dev\\tera\\tera-server-client-data\\100.02\\Datasheet\\CompensationData")
//    .Where(f => f.Contains("QuestCompensationData"));


//var xdocument = XDocument.Load(File.OpenRead("D:\\dev\\tera\\tera-server-client-data\\100.02\\DC\\EUR\\QuestCompensationData\\QuestCompensationData-3.xml"));

//var nav = xdocument.CreateNavigator();
//nav.MoveToFollowing(XPathNodeType.Element);
//var namespaces = nav.GetNamespacesInScope(XmlNamespaceScope.All);

//var compesations = new ConcurrentBag<QuestCompensationData>();


//var ss = new XmlSerializer(typeof(List<Item>));
//var itemss = new List<Item>
//{
//    new() { Class = "sorcerer", Quantity = 3, TemplateId = 19488 },
//    new() { Class = "archer", Quantity = 3, TemplateId = 19488 },
//    new() { Class = "elementalist", Quantity = 3, TemplateId = 19555 },
//};


//var sb = new StringBuilder();
//var sr = new StringWriter(sb);
//var ns = new XmlSerializerNamespaces();
//ns.Add(string.Empty, string.Empty);
//var xw = XmlWriter.Create(sr, new XmlWriterSettings
//{
//    OmitXmlDeclaration = true,
//    Indent = true
//});
//ss.Serialize(xw, itemss.AsEnumerable(), ns);


//var t = sb.ToString();
//var parent = XElement.Parse(t);


//Console.WriteLine("Loading files...");

////foreach (var path in files)
////{
////    using var fs = File.OpenRead(path);
////    var s = new XmlSerializer(typeof(QuestCompensationData));
////    var questData = (QuestCompensationData?)s.Deserialize(fs);
////    if (questData != null)
////        compesations.Add(questData);
////}


//var fileName = "D:\\dev\\tera\\tera-server-client-data\\100.02\\Datasheet\\CompensationData\\QuestCompensationData_10.xml";
//await using var fs = File.OpenRead(fileName);
//var xdoc = await XDocument.LoadAsync(fs, LoadOptions.PreserveWhitespace, CancellationToken.None);

//var coiso = xdoc.XPathSelectElements("./QuestCompensationData/Quest");
//var items = from quest in xdoc.Descendants("Quest")
//            select new
//            {
//                QuestId = quest.Attribute("questId")?.Value,
//                Compensations = from compensation in quest.Descendants("Compensation")
//                                let compensationType = compensation.Descendants("CompensationType").FirstOrDefault()
//                                select new
//                                {
//                                    CompensationId = compensation.Attribute("compensationId")?.Value,
//                                    Type = compensationType.Attribute("type")?.Value,
//                                    Exp = compensationType.Attribute("exp")?.Value,
//                                    Gold = compensationType.Attribute("gold")?.Value,
//                                    Memo = compensationType.Attribute("memo")?.Value,
//                                    PolicyPoint = compensationType.Attribute("policyPoint")?.Value,
//                                    NpcGuildId = compensationType.Attribute("npcGuildId")?.Value,
//                                    ReputationExp = compensationType.Attribute("reputationExp")?.Value,
//                                    ReputationPoint = compensationType.Attribute("reputationPoint")?.Value,
//                                }
//                                //Compensations = quest.Descendants("Compensation").Select(compensation => new
//                                //{
//                                //    CompensationId = compensation.Attribute("compensationId")?.Value
//                                //})
//            };


//var compensations = xdoc.Descendants("CompensationType");
//foreach (var compensation in compensations)
//{
//    compensation.Attribute("exp")?.SetValue("666");
//    compensation.Add(
//        new XText(new string(' ', 2)),
//        new XElement("Items",
//            new XAttribute("templateId", 1234),
//            new XAttribute("quantity", 1234)),
//        new XText(Environment.NewLine),
//        new XText(new string(' ', 6)));
//}

//await using var fsw = new FileStream(@"D:\QuestCompensationData_10.xml", FileMode.Create, FileAccess.Write, FileShare.Write, 1024);
//var xmlSettings = new XmlWriterSettings
//{
//    Indent = true,
//    IndentChars = "  ",
//    Async = true,
//    Encoding = Encoding.UTF8
//};
//await using var xmlw = XmlWriter.Create(fsw, xmlSettings);
//await xdoc.SaveAsync(xmlw, CancellationToken.None);
//await xmlw.FlushAsync();

//Parallel.ForEach(files, path =>
//{
//    using var fs = File.OpenRead(path);
//    var s = new XmlSerializer(typeof(QuestCompensationData));
//    var questData = (QuestCompensationData?)s.Deserialize(fs);
//    if (questData != null)
//        compesations.Add(questData);
//});

//Console.WriteLine($"Loaded: {compesations.Count}");


//var fileName = "D:\\dev\\tera\\tera-server-client-data\\100.02\\Datasheet\\CompensationData\\QuestCompensationData_10.xml";
//using var fs = File.OpenRead(fileName);
//var s = new XmlSerializer(typeof(QuestCompensationData));
//var questData = (QuestCompensationData?)s.Deserialize(fs);
//using var fsw = File.Create(@"D:\teste.xml");
//s.Serialize(fsw, questData);

//var reps = compesations.Where(c => c.Quest != null).SelectMany(c => c.Quest).Where(q => q.Compensation.CompensationType.ReputationPoint)

//Console.ReadKey();