# Tera Xml Tools

Tera Xml Tools is a desktop software designed for Windows that allows users to easily read and modify Tera's Datacenter and Server XML files.

## Important

The software still in early development and it might corrupt your files, make sure you backup your files.

## How to Use

1. Download and install .NET 6 from the official Microsoft website: https://dotnet.microsoft.com/en-us/download/dotnet/6.0
2. Update the server and client folders in the appsettings.json file with the appropriate information.

## About the Software Architecture

Tera Xml Tools is developed using C# .NET 6 and follows a modular architecture, with two main layers: the UI Layer and the Service Layer.

The UI Layer is implemented using WPF and the MVVM Pattern, with the help of the PRISM framework. The Service Layer uses MediatR and a Repository Pattern to manage data access and retrieval.

The current data source implementation is the InMemoryData, which uses lazy loading and the XmlSerializer class to deserialize XML data into memory.
